# Function used to read the discharge Sample files

# Load the required workbook
def findVfromSOC(dischargeSamplefile,SOC,Z):
    from openpyxl import load_workbook
    wb = load_workbook(dischargeSamplefile)
    ws = wb['Sheet1']
    import numpy
    # Define the lists that will store all the values
    voltage_array = numpy.array([])
    current_array = numpy.array([])
    chargecapacity_array = numpy.array([])

    # Go through each cell and extract the useful information
    for row in ws.iter_rows():
        for cell in row:
            # Ignore the title row
            if cell.row > 1:
                # Pick out the values we want
                if cell.column == 'B':
                    # This represents the voltage in V
                    voltage_array = numpy.append(voltage_array, float(cell.value))
                if cell.column == 'C':
                    # This represents the current in mA
                    c_val = float(cell.value)
                    c_val = c_val*(10**-3)
                    current_array = numpy.append(current_array,c_val)
                if cell.column == 'D':
                    # This represents the charge capacity in mAh
                    chargecapacity = float(cell.value)*(10**-3)
                    chargecapacity_array = numpy.append(chargecapacity_array, chargecapacity)

    ## Now we convert charge capacity to SOC

    # First find the unique values for charge capacity and the corresponding voltage and currents
    unique_chargecapacity, indices = numpy.unique(chargecapacity_array,return_index=True)
    corres_current = current_array[indices]
    corres_voltage = voltage_array[indices]

    # Find the max charge and use it to convert the charge capacity into relative charge capacity
    max_chargecapacity = max(unique_chargecapacity)
    relative_chargecapacity = (max_chargecapacity-unique_chargecapacity)/max_chargecapacity
    # The relative charge capacity is the SOC
    import scipy
    import scipy.interpolate

    # Define the interpolant
    voltage_interpolant = scipy.interpolate.RegularGridInterpolator((relative_chargecapacity,),corres_voltage)
    current_interpolant = scipy.interpolate.RegularGridInterpolator((relative_chargecapacity,),corres_voltage)

    # Define the point
    pt = numpy.array([SOC])

    # Use the interpolation to find the voltage and current
    u0 = voltage_interpolant(pt)
    i0 = current_interpolant(pt)

    # Find V
    V = u0 + i0*Z

    # Return value
    return V





