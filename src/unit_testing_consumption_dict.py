# Unit testing function to obtain the consumption dictionaries for the different power conditions
from utils import get_conso_dict
power_condition = 'SIMPLE'
sat_mode_file = 'SatelliteModes.csv'
IDM_FILE = 'UHF_powerconsumption.xlsx'
power_condition_file = 'InputPowerConditioning.xlsx'

if power_condition == 'SIMPLE':
    file = IDM_FILE
else:
    file = power_condition_file

conso_dictionary = get_conso_dict(sat_mode_file, file, power_condition)

print(conso_dictionary)

power_condition = "POWERCONVERTER+BUSES"

if power_condition == 'SIMPLE':
    file = IDM_FILE
else:
    file = power_condition_file

conso_dictionary = get_conso_dict(sat_mode_file, file, power_condition)

print(conso_dictionary)