class battery:
    power_in = 0
    voltage = 0
    current_in = 0
    accumulator = 0
    soc_saved = 0
    volt_saved = 0
    current_saved = 0
    bat_temp = 0


    # Temp properties for obtaining values
    time_v_list = []
    time_a_list = []

    def __init__(self, Temperature_array, C0, SOC0, p, s, total_cycles_applied, meanDOD0, meanSOC0, age, meantempbefore,
                 internalImpedance, lastMaxSOC, datasheets = []):
        self.p  = p # No of accumulator cells in parallel
        self.s = s # No of accumulator cells in series
        self.T_array = Temperature_array
        self.C0 = C0 # Maximum Theoretical Capacity of Single Accumulator
        self.totalcycles0 = total_cycles_applied # Total cycles already applied
        self.meanDOD0 = meanDOD0 # Current mean DOD0 (based on max - min)
        self.SOC = SOC0 # Starting SOC
        self.meanSOC0 = meanSOC0 # Mean SOC over previous life of battery
        self.age = age # Current age of battery in years
        self.age0 = age
        self.meantempbefore = meantempbefore # Average temperature when used before
        self.intZ = internalImpedance
        self.lastMaxSOC = lastMaxSOC
        self.datasheets = []
        import os
        for datasheet in datasheets:
            self.datasheets.append(os.path.join("dataFiles", datasheet))

    def accumulatorsetup(self,dischargefile):
        from accumulator import singleaccumulator
        import os
        dischargefile = os.path.join("dataFiles", dischargefile)
        self.accumulator = singleaccumulator(dischargefile,self.SOC,self.age,self.intZ,self.meantempbefore,
                                             self.meanSOC0, self.totalcycles0, self.meanDOD0, self.C0, self.lastMaxSOC,
                                             datasheets=self.datasheets)

    def batterytick(self,position,powerintobattery,timeinyears):
        # Define the attributes
        ageBefore = self.age
        self.age = self.age0 + timeinyears

        timestepinyears = self.age - ageBefore
        timestepinseconds = timestepinyears * 365.25 * 24 * 60 * 60

        self.accumulator.SOC = self.accumulator.findSOC(timestepinseconds)
        self.soc_saved = self.accumulator.SOC
        #print('SOC saved is '+str(self.soc_saved))

        # Get temperature of the battery
        T_now = self.T_array[position]
        self.bat_temp = T_now

        # Define the input power
        self.power_in = powerintobattery
        import time
        start = time.time()
        # Get the voltage from the accumulator
        self.voltage = self.s * self.accumulator.findVfromSOC()
        end = time.time()
        self.time_v_list.append(end-start)
        #print('time to find voltage from SOC '+str(max(self.time_v_list))+'s')
        #print('voltage in battery tick '+str(self.voltage))
        # Find the current into the battery
        self.current_in = -(powerintobattery)/self.voltage
        #print('power in battery is '+str(powerintobattery))
        #print('current into battery '+str(self.current_in))
        # Therefore current into each cell is
        currentcell = self.current_in/self.p
        start = time.time()
        # Call the accumulator tick
        self.accumulator.accumulatortick(timeinyears, currentcell, T_now)
        end = time.time()
        self.time_a_list.append(end - start)
        #   print('time taken for accumulator tick '+str(max(self.time_a_list))+'s')

    def getSOC(self):
        soc = self.soc_saved
        return soc

    def getCurrent(self):
        # Get the voltage
        V = self.voltage
        # Get the current
        I = self.current_in
        return I, V











