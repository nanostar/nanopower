# Unit testing function for obtaining the mission plan
import configparser
from utils import missionPlan_excel
# Read the configparser
config = configparser.ConfigParser()
config.read('example.ini')
orbit_period = config['Time Array'].getfloat('Period')
# Define the mission plan via excel
mission_file = config['Mission Plan Model']['Mission plan file']
mode_start_time = missionPlan_excel(mission_file, orbit_period)
print(mode_start_time)