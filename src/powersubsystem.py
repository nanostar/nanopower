class powersubsystem_king:
    from solarcell import janus_solarcell
    from diode import diode
    requiredpowernow = 0
    inputpowernow = 0
    software_bat_mode = 'NORMAL' # 2 modes either normal or full
    margin = 0
    in_voltagenow = 0
    converters = []
    pvconverter = 0
    timenow = 0
    panel_definition = []

    # list for storing
    listn = []
    listb = []
    listba = []
    listn1 = []
    mane = []

    def __init__(self, solarangle, eclipse_indicator,conso_dict,mission_plan,timing,total_number_of_cells,temperature,maxcells,tot_panels,EPS,battery,solararrangement,extramodes = [],solarcellclass=janus_solarcell(),the_diode=diode()):
        import numpy
        self.solarangle = solarangle
        self.eclipse_indicator = eclipse_indicator
        self.conso_dict = conso_dict
        self.time = timing
        self.EPS = EPS
        self.battery = battery
        self.totalpanels = tot_panels
        self.missionplan = mission_plan
        self.totalcells = total_number_of_cells
        self.temperature = temperature
        self.solararrangement = solararrangement
        self.extramodes = extramodes
        self.solarcell = solarcellclass
        self.thediode = the_diode
        self.no_of_channels = len(self.solararrangement)
        self.max_cells = maxcells
        self.destroyed_p = numpy.zeros(self.no_of_channels)
        # Obtain all the modes in a nice list
        self.mode_list = self.createsinglemodelist()

    def show(self):
        print('solar arrangement is '+str(self.solararrangement))
        print('required power is '+str(self.requiredpowernow))
        print('input power is '+str(self.inputpowernow))
        print('panel defn is '+str(self.panel_definition))

    def tickonwards(self, position):

        # Define the current values for solar angle, eclipse indicator and time
        solarangle_now = self.solarangle[position,:]
        eclipseind_now = self.eclipse_indicator[position,:]
        time_now = self.time[position]
        temp_now = self.temperature[position]
        self.set_destroyed_cells()
        self.timenow = time_now
        # Set time in years
        timeinyears = time_now/(365.25*24*60*60)
        print(solarangle_now)
        # Sort out the eclipse indication and solar arrangement
        import time
        start = time.time()
        eclipseindication, solarangleindication = self.organise(eclipseind_now, solarangle_now)
        end = time.time()
        self.listb.append(end-start)
        print('time taken for organising '+str(max(self.listb))+'s')
        # Find the required power
        start = time.time()
        self.findrequiredpower(time_now)
        self.requiredpowernow = float(self.requiredpowernow)
        #print('required power is '+str(self.requiredpowernow))
        end = time.time()
        self.listn.append(end-start)
        print('time taken to find required power ' + str(max(self.listn)) + 's')
        start = time.time()
        #print('Required power is '+str(self.requiredpowernow))
        # Find the input power
        self.findinputpower(solarangleindication,eclipseindication,time_now,temp_now)
        #print('input power is ' + str(self.inputpowernow))
        end = time.time()
        self.listn1.append(end - start)
        print('time taken to find input power ' + str(max(self.listn1)) + 's')
        start = time.time()
        #print('input power is '+ str(self.inputpowernow))
        # Call tick for the battery which will update the capacity degradation and find battery SOC
        self.battery.batterytick(position,self.requiredpowernow-self.inputpowernow,timeinyears)
        end = time.time()
        self.listba.append(end - start)
        print('time taken for battery tick '+ str(max(self.listba))+'s')
        # Update the battery modes
        start = time.time()
        self.update_bat_modes()
        end = time.time()
        self.mane.append(end - start)
        print('time taken for updating battery modes '+str(max(self.mane))+'s')
        return self

    def set_destroyed_cells(self):
        import numpy
        # Edit the below code to account for destroyed cells
        self.destroyed_p = numpy.zeros(self.no_of_channels)

    def findrequiredpower(self,time_now):
        import numpy
        import math
        # Check the extra battery modes if there is a hardware condition note: extra modes are already sorted
        power_already_set = False
        for extramode in self.extramodes:
            if not(power_already_set):
                # Check if state is ON
                #print(extramode.state)
                #print(extramode.require_yn)
                if extramode.state == 'ON' and extramode.require_yn:
                    # If there is we want to compare the condition
                    for tocheck in extramode.req_effect:
                        if tocheck[0] == 'Voltage':
                            print('Gotta Fix this part up later')
                        if tocheck[0] == 'Current':
                            print('Again Gotta Fix this later')
                        if tocheck[0] == 'Power':
                            # Set the required power to the value provided
                            self.requiredpowernow = tocheck[1]
                            power_already_set = True

        if not(power_already_set):
            # Now we have checked the extra modes, normal or full modes will not effect power, hence required power is
            # given by the conso dictionary
            for j in list(range(0,len(self.missionplan))):
                # Set the current mission mode and starting time
                mission_mode = self.missionplan[j][0]
                mission_start_time = self.missionplan[j][1]

                # Set the next mission time, not if at end of array then next stop will make it larger than the array can
                # handle
                if j+1 < len(self.missionplan):
                    next_mission_start_time = self.missionplan[j+1][1]
                else:
                    next_mission_start_time = math.inf

                if mission_start_time <= time_now < next_mission_start_time:
                    current_mode = mission_mode
                    if self.EPS.power_conditioning == 'SIMPLE':
                        # Now find the consumption based on conso dictionary
                        self.requiredpowernow = self.conso_dict[current_mode]
                        self.requiredpowernow = self.requiredpowernow * (1+self.margin)
                        power_already_set = True
                        #print('Required Power is ' + str(self.requiredpowernow))
                    else:
                        # We have power conditioning with converters
                        self.requiredpowernow = 0
                        for k in numpy.arange(0,len(self.converters),1):
                            converter = self.converters[k]

                            # Find the required power from this converter
                            converter_conso_dict = self.conso_dict[k]
                            req_power_after_converter = converter_conso_dict[current_mode]
                            #print(req_power_after_converter)
                            #print('----')

                            # Find the efficiency based on this power
                            converter.interpolate_for_efficiency(req_power_after_converter)
                            eta = converter.efficiency

                            # Find the required power
                            self.requiredpowernow += req_power_after_converter/eta
                        #print('Required Power is '+str(self.requiredpowernow))


        #print('Required Power is '+str(self.requiredpowernow))
        return self

    def findinputpower(self,solarangle_now,eclipseind_now,time_now,temp_now):
        # This function finds the input power produced by the solar panels
        power_already_set = False
        pvLine_list = []
        # Go through each mode and check if they have an input effect
        for extramode in self.extramodes:
            if not(power_already_set):
                # Check if the extramode is on
                if extramode.state == 'ON' and extramode.input_yn:
                    # If so check the input condition
                    for tocheck in extramode.in_effect:
                        if tocheck[0] == 'Voltage':
                            # Set the input voltage now to be
                            self.in_voltagenow = tocheck[1]

                            # Find the power based on the above voltage
                            total_power = 0
                            for k in list(range(0,self.no_of_channels)):
                                pv_object = self.get_pvLine_object(k,temp_now,solarangle_now[k],eclipseind_now[k])
                                total_power += pv_object.power_now
                                # Update each pvLine object with the
                                pvLine_list.append(pv_object)
                            self.inputpowernow = total_power
                            power_already_set = True
                        if tocheck[0] == 'Current':
                            # Define the current
                            current_in = tocheck[1]
                            # How does fixed current work??
                            print('Again Gotta Fix this later')
                        if tocheck[0] == 'Power':
                            # Set the required power to the value provided
                            self.inputpowernow = tocheck[1]
                            power_already_set = True
        # If now assume we are in normal mode, use MPPT to find the power (if power isnt already set)
        if not(power_already_set):
            # For each channel run the MPPT function
            totalpower = 0
            #print('initial total power is ' + str(totalpower))
            for k in list(range(0,self.no_of_channels)):
                # Obtain pvObject via MPPT
                pv_object = self.get_pvLine_object_MPPT(k, temp_now, solarangle_now[k], eclipseind_now[k])
                #print(eclipseind_now[k])
                #print('power in a channel is ' + str(pv_object.power_now))
                totalpower += pv_object.power_now
                #print('total power as a sum is '+str(totalpower))
            self.inputpowernow = totalpower
        #print(self.inputpowernow)
        # However we still have to check if the battery is in full mode and reduce the power
        if self.software_bat_mode == 'FULL':
            #print('test101')
            # This is only required if the input power is larger than the required power
            #print(self.inputpowernow)
            #print(self.requiredpowernow)
            if self.inputpowernow > self.requiredpowernow:
                # Define the lower factor
                loweringfactor = self.requiredpowernow/self.inputpowernow
                # Only need this to update the pvLine inputs -> still to do, depending later requirements

                # Define the input power as the required power
                self.inputpowernow = self.requiredpowernow
        return self

    def get_pvLine_object(self,k,temp_now,solarangle_now,eclipseind):
        from pvLine import get_pVobject_from_voltage
        s = self.solararrangement[k][0]
        p = self.solararrangement[k][1]
        T = temp_now
        voltage_req = self.in_voltagenow
        destroyed_cells = self.destroyed_p[k]
        # Check eclipse indicator and solar angles dont create any issues
        #(solarangle,eclipseind) = check_arrangement(eclipseind,solarangle_now)
        return get_pVobject_from_voltage(s,p,voltage_req,T,destroyed_cells,solarangle_now,eclipseind,self.solarcell,self.thediode,self.pvconverter)

    def get_pvLine_object_MPPT(self,k,T,solarangle,eclispeind):
        from pvLine import get_pVobject_MPPT
        s = self.solararrangement[k][0]
        p = self.solararrangement[k][1]
        return get_pVobject_MPPT(s,p,T,self.destroyed_p[k],solarangle,eclispeind,self.solarcell,self.thediode,self.EPS, self.pvconverter)

    def update_bat_modes(self):
        import warnings
        # Find the voltage; current and power in
        #print('battery mode check')
        current, voltage = self.battery.getCurrent()
        power_in = -current*voltage
        #print('voltage reading is ' + str(voltage))
        for checkmode in self.mode_list:
            #print(checkmode)
            #for extra in self.extramodes:
             #   print(extra.state)
            # Check if the mode is FULL
            if checkmode == 'FULL':
                if self.software_bat_mode == 'FULL':
                    # Check if we are to exit full and into normal (battery is define to be full by its tension)
                    if voltage < self.EPS.Vmax:
                        # Voltage is less than the full voltage, so the software mode is normal
                        self.software_bat_mode = 'NORMAL'
            # Also check if we are NORMAL
            elif checkmode == 'NORMAL':
                if self.software_bat_mode == 'NORMAL':
                    # Check if we are able to enter FULL
                    if voltage >= self.EPS.Vmax:
                        # Send the software bat mode into FULL
                        self.software_bat_mode = 'FULL'
                    # Otherwise we want to check all extramodes
                    else:
                        # Check for the extraeffectnormal condition - if in normal assumes the following state is already off
                        for extramode in self.extramodes:
              #              print('checking each mode')
                            if extramode.extra_effect_norm:
                                # Need to check the state - this is the weird bit because technically these are software modes - ignoring for now
                                # We are seeing if we can enter the mode
                                for entrycondition in extramode.entry_condition:
                                    # Check the first element
                                    if entrycondition[0] == 'Voltage':
                                        # print('extra with normal')
                                        # Run the execute with the condition set
                                        combine_str = str(voltage) + entrycondition[1]
               #                         print(combine_str)
                                        # print(combine_str)
                                        # Evaluate this string
                                        if eval(combine_str):
                                            # This state is now on
                                            extramode.state = 'ON'
                #                            print('state is '+str(extramode.state))
                                            # Set the software mode to this
                                            self.software_bat_mode = extramode.name

                                            # Raise the warning
                                            warnings.warn("Entered Mode "+extramode.name)
                                    if entrycondition[0] == 'Current':
                                        # Run the execute with the condition set
                                        combine_str = str(current) + entrycondition[1]
                                        # print(combine_str)
                                        # Evaluate this string
                                        if eval(combine_str):
                                            # This state is now on
                                            extramode.state = 'ON'
                                            # Set the software mode to this
                                            self.software_bat_mode = extramode.name
                                            # Raise the warning
                                            warnings.warn("Entered Mode " + extramode.name)

                                    if entrycondition[0] == 'Power':
                                        # Run the execute with the condition set
                                        combine_str = str(power_in) + entrycondition[1]
                                        # print(combine_str)
                                        # Evaluate this string
                                        if eval(combine_str):
                                            # This state is now on
                                            extramode.state = 'ON'
                                            # Set the software mode to this
                                            self.software_bat_mode = extramode.name
                                            # Raise the warning
                                            warnings.warn("Entered Mode " + extramode.name)
                                    if entrycondition[0] == 'Time':
                                        # Run the execute with the condition set
                                        combine_str = str(self.timenow) + entrycondition[1]
                                        # print(combine_str)
                                        # Evaluate this string
                                        if eval(combine_str):
                                            # This state is now on
                                            extramode.state = 'ON'
                                            # Set the software mode to this
                                            self.software_bat_mode = extramode.name
                                            # Raise the warning
                                            warnings.warn("Entered Mode " + extramode.name)
                                    if entrycondition[0] == 'Temperature':
                                        # Run the execute with the condition set
                                        combine_str = str(self.battery.bat_temp) + entrycondition[1]
                                        # print(combine_str)
                                        # Evaluate this string
                                        if eval(combine_str):
                                            # This state is now on
                                            extramode.state = 'ON'
                                            # Set the software mode to this
                                            self.software_bat_mode = extramode.name
                                            # Raise the warning
                                            warnings.warn("Entered Mode " + extramode.name)

                #for extra in self.extramodes:
                 #   print(extra.state)
            else:
                if checkmode.extra_effect_norm:
                    if checkmode.state == 'ON':
                        # We want to see if we can exit this mode
                        for exitcondition in checkmode.exit_condition:
                            # Check the first element
                            if exitcondition[0] == 'Voltage':
                                # Now we need the voltage
                                combine_str = str(voltage) + exitcondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # Now the mode is OFF
                                    checkmode.state = 'OFF'
                                    # Send the mode back to normal
                                    self.software_bat_mode = 'NORMAL'
                            if exitcondition[0] == 'Current':
                                # Now we need the voltage
                                combine_str = str(current) + exitcondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # Now the mode is OFF
                                    checkmode.state = 'OFF'
                                    # Send the mode back to normal
                                    self.software_bat_mode = 'NORMAL'
                            if exitcondition[0] == 'Power':
                                # Now we need the voltage
                                combine_str = str(power_in) + exitcondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # Now the mode is OFF
                                    checkmode.state = 'OFF'
                                    # Send the mode back to normal
                                    self.software_bat_mode = 'NORMAL'
                            if exitcondition[0] == 'Time':
                                # Now we need the voltage
                                combine_str = str(self.timenow) + exitcondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # Now the mode is OFF
                                    checkmode.state = 'OFF'
                                    # Send the mode back to normal
                                    self.software_bat_mode = 'NORMAL'
                            if exitcondition[0] == 'Temperature':
                                # Now we need the voltage
                                combine_str = str(self.battery.bat_temp) + exitcondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # Now the mode is OFF
                                    checkmode.state = 'OFF'
                                    # Send the mode back to normal
                                    self.software_bat_mode = 'NORMAL'
                else:
                    # Check if the mode is OFF
                    if checkmode.state == 'OFF':
                        # Now check the entry condition
                        for entrycondition in checkmode.entry_condition:
                            # Check the first element
                            if entrycondition[0] == 'Voltage':
                                # Run the execute with the condition set
                                combine_str = str(voltage) + entrycondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # This state is now on
                                    checkmode.state = 'ON'
                                    # Raise the warning
                                    warnings.warn("Entered Mode " + checkmode.name)
                            if entrycondition[0] == 'Current':
                                # Run the execute with the condition set
                                combine_str = str(current) + entrycondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # This state is now on
                                    checkmode.state = 'ON'
                                    # Raise the warning
                                    warnings.warn("Entered Mode " + checkmode.name)
                            if entrycondition[0] == 'Power':
                                # Run the execute with the condition set
                                combine_str = str(power_in) + entrycondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # This state is now on
                                    checkmode.state = 'ON'
                                    # Raise the warning
                                    warnings.warn("Entered Mode " + checkmode.name)
                            if entrycondition[0] == 'Time':
                                # Run the execute with the condition set
                                combine_str = str(self.timenow) + entrycondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # This state is now on
                                    checkmode.state = 'ON'
                                    # Raise the warning
                                    warnings.warn("Entered Mode " + checkmode.name)
                            if entrycondition[0] == 'Temperature':
                                # Run the execute with the condition set
                                combine_str = str(self.battery.bat_temp) + entrycondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # This state is now on
                                    checkmode.state = 'ON'
                                    # Raise the warning
                                    warnings.warn("Entered Mode " + checkmode.name)
                            # Otherwise it remains being OFF
                    # Otherwise the mode is ON
                    else:
                        # Now need to check the exit condition
                        for exitcondition in checkmode.exit_condition:
                            # Check the first element
                            if exitcondition[0] == 'Voltage':
                                # Now we need the voltage
                                combine_str = str(voltage) + exitcondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # Now the mode is OFF
                                    checkmode.state = 'OFF'
                            if exitcondition[0] == 'Current':
                                # Run the execute with the condition set
                                combine_str = str(current) + exitcondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # This state is now on
                                    checkmode.state = 'ON'
                            if exitcondition[0] == 'Power':
                                # Run the execute with the condition set
                                combine_str = str(power_in) + exitcondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # This state is now on
                                    checkmode.state = 'ON'
                            if exitcondition[0] == 'Time':
                                # Run the execute with the condition set
                                combine_str = str(self.timenow) + exitcondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # This state is now on
                                    checkmode.state = 'ON'
                            if exitcondition[0] == 'Temperature':
                                # Run the execute with the condition set
                                combine_str = str(self.battery.bat_temp) + exitcondition[1]
                                # Evaluate this string
                                if eval(combine_str):
                                    # This state is now on
                                    checkmode.state = 'ON'

        #print('checking extramodes')
        #for extramode in self.extramodes:
         #   print(extramode.state)

    def organise(self, eclipseind_now, solarangle_now):
        eclipseindication = []
        solarindication = []
        #print(self.panel_definition)
        for val1 in self.panel_definition:
            eclip1 = []
            solar1 = []
            for val in val1:
                #print('val is '+str(val))
                for j in list(range(0, self.totalpanels)):
                    if j == val:
                        #print(j)
                        eclip1.append(eclipseind_now[j])
                        #print(eclip1)
                        solar1.append(solarangle_now[j])
            eclipseindication.append(eclip1)
            solarindication.append(solar1)
        return eclipseindication, solarindication

    def set_converters(self,converter_list):
        self.converters = converter_list

    def set_pv_converters(self, pvconverter):
        self.pvconverter = pvconverter

    def createsinglemodelist(self):
        # Create another mode array by concatenating the hardware modes + Full + Normal + Effect Normal + Itself modes
        hardware_modes = []
        effect_normal = []
        effect_itself = []
        for extramode in self.extramodes:
            if extramode.hardware_yn:
                hardware_modes.append(extramode)
            elif extramode.extra_effect_norm:
                effect_normal.append(extramode)
            else:
                effect_itself.append(extramode)
        # Define all the modes in order we are checking them
        modes = hardware_modes + ['FULL', 'NORMAL'] + effect_normal + effect_itself
        return modes

    def set_panel_def(self, set_panel_defn, panel_defn):
        from utils import paneldef
        # Sort out the eclipse indication and solar arrangement
        if set_panel_defn:
            self.solararrangement, self.panel_definition = paneldef(self.solararrangement, self.max_cells,
                                                                    self.totalpanels)
        else:
            self.panel_definition = panel_defn












