# Unit testing function for the power subsystem organise function

# First define the power subsystem
import numpy
import math
import matplotlib.pyplot as plt
from utils import get_conso_dict, missionPlan_excel, find_solar_eclipse_matrix
from EPS import EPS
from batterymode import obtainadditionalmodes
from battery import battery
from powersubsystem import powersubsystem_king
from converter import get_converter_object, getpvconverterobject
import configparser
import time
start = time.time()
# Read the configparser
config = configparser.ConfigParser()
config.read('example2.ini')

test_time = config['Time Array'].getfloat('Test time')  # seconds
orbit_period = config['Time Array'].getfloat('Period')
increment = config['Time Array'].getfloat('Increment')  # seconds
test_time_array = numpy.arange(0, test_time, increment)
no_of_panels = config['Panel and Solar Arrangement'].getint('Number of panels')
total_cells = config['Panel and Solar Arrangement'].getint('Total cells')

# Define the solar arrangement
solararrangement = config['Panel and Solar Arrangement']['Solar arrangement']
solararrangement = solararrangement.split(';')
for i in list(range(0, len(solararrangement))):
    single_arrange = solararrangement[i].split(',')
    for j in list(range(0, len(single_arrange))):
        single_arrange[j] = int(single_arrange[j])
    solararrangement[i] = single_arrange

# Set the power conditioning
power_condition = config['Power Conditioning Properties']['Power condition']

# Max Voltage of Battery
Vmax = config['EPS'].getfloat('Battery Voltage Max')
# Increment for MPPT
MPPT_increment = config['EPS'].getfloat('MPPT Increment')
eps = EPS(Vmax, MPPT_increment, power_condition)

# Define the max cells and total number of panels
tot_panels = no_of_panels
max_cells = config['Panel and Solar Arrangement']['Max cells']
max_cells = max_cells.split(',')
for j in list(range(0, len(max_cells))):
    max_cells[j] = int(max_cells[j])


OEM_file = config['Satellite and SUN OEM and AEM']['Sat OEM File']
AEM_file = config['Satellite and SUN OEM and AEM']['Sat AEM File']
Sun_OEM_file = config['Satellite and SUN OEM and AEM']['Sun OEM File']
panel_normal_file = config['Panel and Solar Arrangement']['Panel Normal File']
x_rep_file = config['Satellite and SUN OEM and AEM']['Body x representation in relative basis file']
solar_angle_matrix, eclipse_indicator = find_solar_eclipse_matrix(no_of_panels, OEM_file, AEM_file, Sun_OEM_file,
                                                                  panel_normal_file, x_rep_file)
solar_angle_matrix = solar_angle_matrix.astype(numpy.float)
eclipse_indicator = eclipse_indicator.astype(numpy.float)

# Read mode table from an excel file and consumption table from IDM-CIC file
sat_mode_filename = config['Power Consumption Model']['Satellite mode file']
conso_file = config['Power Consumption Model']['IDM Consumption file']
file_name = config['Power Consumption Model']['Power Conditioning File']
if power_condition == 'SIMPLE':
    file = conso_file
else:
    file = file_name

# Obtain the conso dictionary
conso_dictionary = get_conso_dict(sat_mode_filename, file, power_condition)
# Define the mission plan via excel
mission_file = config['Mission Plan Model']['Mission plan file']
mode_start_time = missionPlan_excel(mission_file, orbit_period)


# develop a test thermal model for each component which varies with time (do it with each component that needs
# a thermal model)
temperature = 50*numpy.ones((len(test_time_array)))
temp_battery = 10*numpy.ones((len(test_time_array)))

# If extra battery modes are required please create the excel sheet as listed in the documentation and fill out the
# following list with the names of the files (note: please ensure files are .csv)
# Define the extra modes in an array
extra_modes = config['Battery Protection Modes']['Extra mode file']
if not(extra_modes == []):
    extra_modes = extra_modes.split(',')
    additional_modes = obtainadditionalmodes(extra_modes)
else:
    additional_modes = extra_modes

# Degradation Model Set up
cyclefile = config['Battery']['Cycle file']
agefile = config['Battery']['Age file']
if not(cyclefile == []):
    datasheets_list = [cyclefile, agefile]
else:
    datasheets_list = []

## Set the battery for the powersubsystem
C0 = config['Battery'].getfloat('Initial Maximum Capacity')  # Capacity of battery in Ah
SOC0 = config['Battery'].getfloat('Initial SOC')
p_bat = config['Battery'].getint('Accumulator Parallel Cells')
s_bat = config['Battery'].getint('Accumulator Series Cells')
accumulator_internal_Z = config['Battery'].getfloat('Accumulator Internal Impedance')
# Previous Life info
meanDOD0 = config['Battery'].getfloat('Mean DOD')
meanSOC0 = config['Battery'].getfloat('Mean SOC')
age0 = config['Battery'].getfloat('Age')
lastMaxSOC = config['Battery'].getfloat('Last Max SOC')
meanTemp0 = config['Battery'].getfloat('Mean Temperature')
cycle0 = config['Battery'].getfloat('Battery Cycles')
bat = battery(temp_battery, C0, SOC0, p_bat, s_bat, cycle0, meanDOD0, meanSOC0, age0, meanTemp0, accumulator_internal_Z,lastMaxSOC,
              datasheets=datasheets_list)
# Accumulator Set up
dischargefile = config['Battery']['Discharge File']
bat.accumulatorsetup(dischargefile)

# Set the solar cell model
# Execute code to find the solar cell model
exec(config['Solar Cell']['Where to get solar cell model'])
# Define the solar cell itself
solar_cell = eval(config['Solar Cell']['Solar Cell Model'])
# Execute code to find the diode model
exec(config['Solar Cell']['Where to get diode properties'])
# Define the diode
the_diode = eval(config['Solar Cell']['Diode'])

# Call the powersubsystem to give it the above properties
power_subsystem = powersubsystem_king(solar_angle_matrix,eclipse_indicator,conso_dictionary,mode_start_time,
                                      test_time_array,total_cells,temperature,max_cells,tot_panels,eps,bat,
                                      solararrangement,extramodes=additional_modes, solarcellclass=solar_cell,
                                      the_diode=the_diode)
panel_define = config['Panel and Solar Arrangement']['Panel Define']
print(panel_define)
if panel_define == '[]':
    # This means we set panel_define
    set_panel_defn = True
else:
    # Otherwise we want to form the panel definition
    set_panel_defn = False
    panel_define_proper = []
    # Split the string according to ;
    panel_per_channel = panel_define.split(';')
    for channel_panel in panel_per_channel:
        panel = []
        # Split this again and go through each element
        for element in channel_panel.split(','):
            # Add the element into panel
            panel.append(int(element))
        panel_define_proper.append(panel)
    panel_define = panel_define_proper
if not(power_condition == 'SIMPLE'):
    # Set up the converters
    bus_efficiency_files = config['Power Conditioning Properties']['Buck Efficiency file']
    #['buck_converter_efficiency3v3.csv', 'buck_converter_efficiency5v.csv']
    bus_efficiency_files = bus_efficiency_files.split(',')
    IV_files = config['Power Conditioning Properties']['IV file']#['VvsI_3_3bus.csv','VvsI_5bus.csv']
    IV_files = IV_files.split(',')

    converter_list = get_converter_object(bus_efficiency_files, IV_files)
    power_subsystem.set_converters(converter_list)

    # Set the input power converters (Optional)
    filename = config['Power Conditioning Properties']['Pv Converter file']
    pv_conv = getpvconverterobject(filename)
    power_subsystem.set_pv_converters(pv_conv)

# Will need to find the initial values for the things I want to plot
SOC_array = numpy.array([])
# Use initial calc on the power subsystem to find the initial current and voltage
power_subsystem.set_panel_def(set_panel_defn, panel_define)
current_array = numpy.array([])
voltage_array = numpy.array([])
req_power_array = numpy.array([])
in_power_array = numpy.array([])
end_uno = time.time()
print('time for set up is '+str(end_uno-start)+'s')

# List for storing time taken
list1 = []
list2 = []
for i in numpy.arange(0, 1, 1):#len(test_time_array), 1):
    print('----')
    print('i is ' + str(i))
    start1 = time.time()
    # Send the position to the power subsystem it knows where to look for information
    power_subsystem.tickonwards(i)
    start2 = time.time()
    list1.append(start2- start1)
    print('time taken for tick onwards '+str(max(list1))+'s')
    # Get SOC for plotting
    soc = power_subsystem.battery.getSOC()
    SOC_array = numpy.append(SOC_array, soc)
    #print('SOC is ' + str(soc))

    # We are also going to add the voltage and current after the tick
    current, voltage = power_subsystem.battery.getCurrent()
    current_array = numpy.append(current_array, current)
    #print('current saved is ' + str(current))

    voltage_array = numpy.append(voltage_array, voltage)
    #print('voltage saved is ' + str(voltage))

    # Get the required power for plotting
    req_power = power_subsystem.requiredpowernow
    req_power_array = numpy.append(req_power_array, req_power)

    # Get the input power for plotting
    in_power = power_subsystem.inputpowernow
    in_power_array = numpy.append(in_power_array, in_power)
    end1 = time.time()
    list2.append(end1-start1)