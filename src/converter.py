class pv_converter:
    def __init__(self,pv_efficiency,input_power,output_power):
        self.pv_efficiency = pv_efficiency
        self.input_power = input_power
        self.output_power = output_power

    def geteffiencyfrominputpower(self,inpower):
        from scipy.interpolate import RegularGridInterpolator
        import numpy as np
        # First sort the array to be in ascending order
        array = self.input_power
        ind = np.unravel_index(np.argsort(array, axis=None), array.shape)
        sorted_in_power = self.input_power[ind]
        sorted_efficiency = self.pv_efficiency[ind]

        # Define the interpolants
        eta_from_inpower = RegularGridInterpolator((sorted_in_power,), sorted_efficiency)
        # Find the efficiency
        efficiency = eta_from_inpower([inpower]).item()
        return efficiency

    def getefficiencyfromoutputpower(self,outpower):
        from scipy.interpolate import RegularGridInterpolator
        import numpy as np
        # First sort the array
        array = self.output_power
        ind = np.unravel_index(np.argsort(array, axis=None), array.shape)
        sorted_out_power = self.output_power[ind]
        sorted_efficiency = self.pv_efficiency[ind]
        # Define the interpolant
        eta_from_outpower = RegularGridInterpolator((sorted_out_power,), sorted_efficiency)
        # Find the efficiency
        efficiency = eta_from_outpower([outpower]).item()
        return efficiency

# Input Pv Converter Efficiency
def getpvconverterobject(filename):
    import csv
    import os
    import numpy as np

    f = open(os.path.join("dataFiles", filename), 'rU')
    reader = csv.reader(f)
    tally = 0
    pv_efficiency = np.array([])
    input_power = np.array([])
    output_power = np.array([])
    for row in reader:
        # Dont read the title
        if tally > 0:
            # Store the data
            pv_efficiency = np.append(pv_efficiency, float(row[1]))
            input_power = np.append(input_power, float(row[0]))
            output_power = np.append(output_power, float(row[0])*float(row[1]))
        tally += 1
    pvconv = pv_converter(pv_efficiency,input_power,output_power)
    return pvconv

class converter:
    efficiency = 0
    def __init__(self,efficiency_data,current_data,I_data,V_data):
        self.efficiencydata = efficiency_data
        self.currentdata = current_data
        self.I_data = I_data
        self.V_data = V_data

    def interpolate_for_efficiency(self,power):
        import numpy as np
        from scipy.interpolate import RegularGridInterpolator
        #print('required power is ' + str(power))

        # Check if power is 0, if this is the case we can just set efficiency to 1
        if power == 0:
            self.efficiency = 1
        else:
            # Find the power via element by element multiplication
            P_data = np.multiply(self.I_data,self.V_data)

            # Second gotta sort out the arrays
            array = self.currentdata
            ind = np.unravel_index(np.argsort(array, axis=None), array.shape)
            sorted_current = array[ind]
            sorted_efficiency = self.efficiencydata[ind]
            array = P_data
            ind = np.unravel_index(np.argsort(array, axis=None), array.shape)
            sorted_P = P_data[ind]
            sorted_I = self.I_data[ind]

            # Now define the Regular Grid Interpolators
            PtoI_interpolant = RegularGridInterpolator((sorted_P,), sorted_I)
            Itoefficiency_interpolant = RegularGridInterpolator((sorted_current,), sorted_efficiency)

            # Find the current based on the interpolant
            current_int = PtoI_interpolant([power])

            # Find the efficiency from the interpolated current
            self.efficiency = Itoefficiency_interpolant(current_int).item()
            #print(self.efficiency)

def get_converter_object(bus_efficiency_files, IV_files):
    import csv
    import os
    #bus_efficiency_files = ['buck_converter_efficiency3v3.csv', 'buck_converter_efficiency5v.csv']
    #IV_files = ['VvsI_3_3bus.csv','VvsI_5bus.csv']
    converter_list = []
    for i in list(range(0, len(bus_efficiency_files))):
        import numpy as np
        tally = 0
        f = open(os.path.join("dataFiles", bus_efficiency_files[i]), 'rU')
        #f = open(bus_efficiency_files[i])
        reader = csv.reader(f)
        current_data = np.array([])
        efficiency_data = np.array([])
        for row in reader:
            # Ignore the first row
            if tally > 0:
                # Save the first column data as the current
                current_data = np.append(current_data, float(row[0]))
                # Save the second column data as the efficiency
                efficiency_data= np.append(efficiency_data, float(row[1]))
            tally += 1
        f.close()
        tally = 0
        f = open(os.path.join("dataFiles", IV_files[i]), 'rU')
        #f = open(IV_files[i])
        reader = csv.reader(f)
        I_data = np.array([])
        V_data = np.array([])
        for row in reader:
            # Ignore the first row
            if tally > 0:
                # Save the first column data as the current
                I_data = np.append(I_data, float(row[0]))
                # Save the second column as the voltage
                V_data=  np.append(V_data, float(row[1]))
            tally += 1
        f.close()

        # Save this in a class
        conv = converter(efficiency_data,current_data,I_data,V_data)

        # Save this converter in a list
        converter_list.append(conv)
    return converter_list



