# Accumulator Class
class singleaccumulator:
    V = 0
    T = 0
    current = 0
    inCharge = False

    listh = []
    def __init__(self,dischargefile,SOC,age,Z,meanT0,meanSOC0,totcycles0,meanDOD0,C0, lastMaxSOC, datasheets = []):
        from tau import tau_class
        self.dischargefile = dischargefile
        self.SOC = SOC
        self.age = age
        self.age0 = age
        self.z = Z # internal Impedance
        self.meanT = meanT0
        self.meanSOC = meanSOC0
        self.totalcycles = totcycles0
        self.meanDOD = meanDOD0
        self.lastmaxSOC = lastMaxSOC
        self.c0 = C0
        self.cm = C0 # Maximum Capacity of the Battery
        if datasheets == []:
            self.mathmodel = True
        else:
            self.mathmodel = False
            self.tau_class = tau_class(datasheets[0], datasheets[1])
        self.datasheets = datasheets

        from openpyxl import load_workbook
        wb = load_workbook(self.dischargefile)
        ws = wb['Sheet1']
        import numpy
        # Define the lists that will store all the values
        voltage_array = numpy.array([])
        current_array = numpy.array([])
        chargecapacity_array = numpy.array([])

        # Go through each cell and extract the useful information
        for row in ws.iter_rows():
            for cell in row:
                # Ignore the title row
                if cell.row > 1:
                    # Pick out the values we want
                    if cell.column == 'B':
                        # This represents the voltage in V
                        voltage_array = numpy.append(voltage_array, float(cell.value))
                    if cell.column == 'C':
                        # This represents the current in mA
                        c_val = float(cell.value)
                        c_val = c_val * (10 ** -3)
                        current_array = numpy.append(current_array, c_val)
                    if cell.column == 'D':
                        # This represents the charge capacity in mAh
                        chargecapacity = float(cell.value) * (10 ** -3)
                        chargecapacity_array = numpy.append(chargecapacity_array, chargecapacity)

        ## Now we convert charge capacity to SOC

        # First find the unique values for charge capacity and the corresponding voltage and currents
        unique_chargecapacity, indices = numpy.unique(chargecapacity_array, return_index=True)
        corres_current = current_array[indices]
        corres_voltage = voltage_array[indices]

        # Find the max charge and use it to convert the charge capacity into relative charge capacity
        max_chargecapacity = max(unique_chargecapacity)
        relative_chargecapacity = (max_chargecapacity - unique_chargecapacity) / max_chargecapacity
        # The relative charge capacity is the SOC
        from scipy.interpolate import RegularGridInterpolator

        # Sort the array
        array = relative_chargecapacity
        ind = numpy.unravel_index(numpy.argsort(array, axis=None), array.shape)
        relative_chargecapacity = array[ind]
        corres_current = corres_current[ind]
        corres_voltage = corres_voltage[ind]

        # Define the interpolant
        self.voltage_interpolant = RegularGridInterpolator((relative_chargecapacity,), corres_voltage)
        self.current_interpolant = RegularGridInterpolator((relative_chargecapacity,), corres_current)


    def findVfromSOC(self):
        import time
        import numpy
        start = time.time()


        # Define the point
        pt = numpy.array([self.SOC])

        # Use the interpolation to find the voltage and current
        v0 = self.voltage_interpolant(pt).item()
        i0 = self.current_interpolant(pt).item()

        # Define u0
        u0 = v0 + i0 * self.z

        #print('u0 is '+str(u0))
        #print('##')
        #print('u0 is '+str(u0))
        #print('current is '+str(self.current))
        # Define the voltage
        self.V = u0 + self.current*self.z
        #print(self.V)
        end = time.time()
        print('time for find V from SOC is '+str(end-start)+'s')
        return self.V

    def accumulatortick(self,timeinyears,currentin,temp):
        # Define the attributes
        ageBefore = self.age
        self.age = self.age0 + timeinyears

        # Set the current
        self.current = currentin
        self.T = temp

        # Set the time step
        timestepinyears = self.age - ageBefore
        timestepinseconds = timestepinyears*365.25*24*60*60


        # Find the new mean temperature and SOC
        if not(timestepinseconds == 0):
            self.meanT = ((self.meanT*ageBefore) + (self.T *timestepinyears))/(ageBefore+timestepinyears)
            self.meanSOC = ((self.meanSOC*ageBefore) + (self.SOC * timestepinyears))/(ageBefore + timestepinyears)
        #print('ageBefore is ' + str(ageBefore))
        #print('time step is '+str(timestepinseconds))

        # If the battery is charging (input power larger than required power i.e. currentin is negative) and previously
        # not charging we up the cycles
        if self.current >= 0:
            if not(self.inCharge):

                # Also want update the meanDOD
                self.meanDOD = ((self.meanDOD*self.totalcycles)+(self.lastmaxSOC-self.SOC))/(self.totalcycles+1)
                #print('mean DOD is '+str(self.meanDOD))
                # Update the total cycles
                self.totalcycles += 1
            # Even if it wasnt previously charging, it is charging now
            self.inCharge = True
        else:
            # Now it is not charging, check if it was previously charging
            if self.inCharge:
                # Save the current SOC as the last max SOC
                self.lastmaxSOC = self.SOC
            # Now in discharge
            self.inCharge = False

        # Find the capacity
        import time
        start = time.time()
        self.findCm()
        end = time.time()
        self.listh.append(end - start)
        print('time for Cm calc is '+str(max(self.listh))+'s')

    def findSOC(self, timestepinseconds):
        #print('current when finding SOC is '+str(self.current))
        # Now find the new SOC
        SOC = self.SOC + ((self.current * (timestepinseconds/3600))/self.cm)
        SOC = max(0, SOC)
        SOC = min(1, SOC)
        return SOC

    def findCm(self):
        # The Mathematical Model is the default model but it has major flaws, please attempt to provide a datasheet
        # model
        if self.mathmodel:
            tau_cycles, tau_age = self.mathematical_tau_calc()
        else:
            # User must have provided 2 datasheets for the calculations
            tau_cycles = self.tau_class.find_tau_cycle(self.meanDOD, self.totalcycles)
            tau_age = self.tau_class.find_tau_age(self.meanT, self.meanSOC, self.age)

        # With tau we can find the Cm value
        self.cm = self.c0*tau_age*tau_cycles

    def mathematical_tau_calc(self):

        # Calculate tau from the effect of cycles, temperature and DOD
        tau_cycles = (110.29 - 0.7551*self.meanT - 0.2977*self.meanDOD*100 -0.0014*self.totalcycles)/100
        tau_cycles = max(0,tau_cycles)
        tau_cycles = min(1,tau_cycles)
        # Tau model for age effect
        tau_age = 1 # Yet to find a nice model for this, have not done a large amount of research on it tho

        return tau_cycles, tau_age









        
