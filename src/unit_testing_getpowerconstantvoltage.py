# unit test function for constant voltage
import math
import numpy
from solarcell import janus_solarcell
from diode import diode
from converter import getpvconverterobject
thediode = diode()

solarcell = janus_solarcell()
voltage_req = 3.7
s = 3
p = 3
eclipseind = [1, 1, 1, 1, 1, 1, 1, 1, 1]
T = 50
solarangle_now = (math.pi/6)*numpy.ones(9)
filename = 'pv_test_data.csv'
pvconverter = getpvconverterobject(filename)
destroyed_p = numpy.zeros(4)
# Test the constant voltage read
from pvLine import get_pVobject_from_voltage
pvLine_object = get_pVobject_from_voltage(s, p, voltage_req, T, destroyed_p, solarangle_now, eclipseind, solarcell, thediode, pvconverter)
print(pvLine_object.current_now)
print(pvLine_object.voltage)
print(pvLine_object.voltage*pvLine_object.current_now)
print(pvLine_object.power_now)

