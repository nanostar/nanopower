from utils import tau_cycle_from_data, tau_age_from_data
DOD = 0.7
SOC = 0.85
T = 10
totcycles =50
age = 0.1
print(tau_cycle_from_data("dataFiles\\accuCyclingData.xlsx", DOD, totcycles))
print(tau_age_from_data("dataFiles\\accuLifeData.xlsx", T, SOC, age))
