class janus_solarcell:
    def __init__(self,T_mid=28,V_max=2.690,I_max=0.5196,V_optimum=2.409,I_optimum=0.5029,dV_maxdT=-6.2e-3,dI_maxdT=0.36e-3,dV_optdT=-6.7e-3,dI_optdT=0.24e-3):
        self.Tmid = T_mid
        self.Vmax = V_max
        self.Imax = I_max
        self.Iopt = I_optimum
        self.Vopt = V_optimum
        self.dImaxdT = dI_maxdT
        self.dVmaxdT = dV_maxdT
        self.dVoptdT = dV_optdT
        self.dIoptdT = dI_optdT
    def calc_current(self,V_req,T,diode,angle):
        # Scale all relevant parameters with temperature
        V_max_with_temp = self.Vmax + (T-self.Tmid)*self.dVmaxdT
        V_opt_with_temp = self.Vopt + (T - self.Tmid) * self.dVoptdT
        I_max_with_temp = self.Imax + (T - self.Tmid) * self.dImaxdT
        I_opt_with_temp = self.Iopt + (T - self.Tmid) * self.dIoptdT

        # Add the diode voltage on
        V_with_diode = V_req + diode.voltage

        # Check if the voltage is less than the optimum voltage
        if V_with_diode < V_opt_with_temp :
            # Set current accordingly
            I = I_max_with_temp + (I_opt_with_temp - I_max_with_temp)*(V_with_diode/self.Vopt)

        else :
            # Otherwise set current according to max voltage
            I = I_opt_with_temp + (0-I_opt_with_temp)*((V_with_diode-V_opt_with_temp)/(V_max_with_temp-V_opt_with_temp))


        # Make sure current is not negative, as solar cells cannot be charged
        if I < 0:
            I = 0

        import math
        #print('angle is '+str(angle))
        # Only account for perpendicular component due to sun angle
        I = I*math.cos(angle)

        return I