Please read the following for instructions on how to use the program and refer to the documention in the Report + Presentation folder
for any specific details. 

This tool simulates a power subsystem and produces the plots for SOC, battery voltage, current, required and input power using the following data files and python files:
1. Sat OEM File: satellite OEM file in an ECI reference frame
2. Sat AEM File: satellite AEM file in quaternions
3. Sun OEM File: sun OEM file in the ECI reference frame
4. Panel Normal File: the normals for each panel in body reference frame
5. Body x representation in relative basis file: the x axis of the body reference in the initial relative ECI (to satellite) reference frame
6. Satellite mode file: File containing the satellite system modes and their corresponding platform and payload modes
7. Discharge Sample File: File containing capacity vs voltage vs current data
8. Mission Plan: Excel based mission plan containing which mode will begin at which time
9. Solar Cell Model: Python class that can calculate current for the cell 
10. Diode: Python class that contain properties that can be used by solar cell model 

This tool requires one of the following data files to model the power consumption 
1. IDM Consumption file: IDM-CIC file converted to excel 
2. Power Conditioning File: File containing the conditioning of the equipment into the different busses

This tool has optional data files which are: 
1. Pv (Boost) Converter Efficiency File: File containing boost converter efficiency vs input power 
2. Buck Converter Efficiency File: FIle containing data for each buck converter efficiency vs current
3. IV File: File containing current vs voltage data for each buck converter  
4. Cycle File: File containing the DOD vs cycles vs tau
5. Age File: File containing the temperature vs SOC vs age vs tau
6. Extra Battery Protection Mode File: Files containing the properties of battery modes that are not FULL or NORMAL

This tool has required values that need to be inputted via the .ini file (refer to report for the structure of these inputs)
1. Test Time: The end time of the simulation (end value in the time array that starts at 0S)
2. Increment: Increment used in the time array
3. Period: Period of the chosen orbit 
4. Total cells: The total number of solar cells on the satellite
5. Max Cells: The total number of cells on each panel in a list 
6. Solar arrangement: The number of series cells and parallel sets in each channel
8. Power Condition: Either "SIMPLE" or "POWERCONVERTERS+BUSSES"
9. Battery Voltage Max: The maximum voltage of the battery before being classified FULL 
10. MPPT Increment: The increment used for MPPT power calculation
11. Initial SOC: Initial value used to begin the simulation 
12. Initial Maximum Capacity: The initial value for battery maximum capacity 
13. Accumulator Series Cells: The number of accumulator cells in series 
14. Accumulator Parallel Cells: The number of the sets of accumulator cells in parallel 
15. Last Max SOC: The last maximum SOC from the battery's previous life 
16. Mean DOD: The mean DOD value from the battery's previous life
17. Mean SOC: The mean SOC value from the battery's previous life 
18. Age: The age (in years) of the battery at the start of the simulation 
19. Mean Temperature: The mean temperature from the battery's previous life 
20. Battery Cycles: The number of cycles the battery has gone through in its previous life

This tool has lists that are optional (refer to report for when this should be inputted)
1. Panel Define: The list representing where the cells of the panels are in reference to the solar arrangement 

-----------------
To use the tool:
-----------------

1. Fill out all the data files required with the values of your power subsystem and save them in the dataFiles folder
1. Open the src folder and the example.ini file
2. Replace values or file names in this file with the values or file names required (refer to report for further information or definitions)
3. Save the .ini file with a specific name in the src folder
4. Open a python editor
5. Navigate to the src folder and open maincode_example.py file 
6. Change the "example.ini" to the specifc name of the .ini file
7. Press Run


Python library Versions Required (code was designed based on these versions):
numpy: 1.16.1
configparser: 3.7.3
matplotlib: 3.0.2
openpyxl: 2.4.1
scipy: 1.2.1

 

 
