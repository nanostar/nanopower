// Author: Chamal Perera
// Aim: This file is used to determine the sun angle to panel of a satellite and the corresponding eclipse indicator 

// Current Goal: Figure out how to use celestlab to get angle to the sun and which vector I would have to use 
// My guess is I would need how

// Start with the basic set up 
xdel(winsid())
exec(fullpath(get_absolute_file_path("Sun_Angle_Eclipse_Indicator.sce")) + "utils/init.sce", -1);
orbits_to_study = [];
orbits_id = fieldnames(orbits);
for i = 1:size(orbits_id,1);
    if orbits_id(i) <> "cjd0" 
        orbits_to_study = [orbits_to_study orbits(orbits_id(i))];
    end
end
orbit = orbits_to_study(:,1);
orbits_id = orbits_id(2); // get rid of the cjd0 thing
//Propagation dates
t_cjd = orbits.cjd0:50/(24*60*60):(orbits.cjd0+11726/(24*60*60));
pos_sun = CL_eph_sun(t_cjd);
pos_sun_sph = CL_co_car2sph(pos_sun);
alpha_sun = pos_sun_sph(1,:);
delta_sun = pos_sun_sph(2,:);
sun_pos_EME = CL_fr_convert("ECI", "EME2000", t_cjd, pos_sun);
sun_pos = sun_pos_EME;

//-// Define each panel based, eventually providing the ECI/EME2000 reference frame unit vectors corresponding to the normal of each  
// Define the body reference frame axis in EME2000 format (set up with x pointing towards the sun)



//Satellite orbit propagation
sat_eph = CL_ex_propagate("eckhech","kep",cjd0,orbit,t_cjd,"m");
[sat_pos_ECI, sat_vel_ECI] = CL_oe_kep2car(sat_eph);
[sat_pos_EME, sat_vel_EME] = CL_fr_convert("ECI", "EME2000", t_cjd, sat_pos_ECI, sat_vel_ECI);
sat_pos = sat_pos_EME;
// param3d1(sat_pos(1,:),sat_pos(2,:),sat_pos(3,:))
relsun_pos_eme = sun_pos_EME-sat_pos_EME;
x_bod = relsun_pos_eme(:,1)/norm(relsun_pos_eme(:,1));
Rz = [0,-1,0;1,0,-1;0,0,1]
y_bod = Rz*x_bod;
Ry = [0,0,1;0,1,0;-1,0,0]
z_bod = inv(Ry)*x_bod
// Define all the panel normals in a body reference frame for satellite 
panel_1_normal = [1;0;0];
panel_2_normal = panel_1_normal//[(2*(rand()>0.5)-1)*rand();(2*(rand()>0.5)-1)*rand();(2*(rand()>0.5)-1)*rand()];
panel_2_normal = panel_2_normal/norm(panel_2_normal);
panel_3_normal = panel_1_normal//[(2*(rand()>0.5)-1)*rand();(2*(rand()>0.5)-1)*rand();(2*(rand()>0.5)-1)*rand()]//[0;1;0];
panel_3_normal = panel_3_normal/norm(panel_3_normal);
panel_4_normal = panel_1_normal//[(2*(rand()>0.5)-1)*rand();(2*(rand()>0.5)-1)*rand();(2*(rand()>0.5)-1)*rand()]
panel_4_normal = panel_4_normal/norm(panel_4_normal);
mat = [x_bod,y_bod,z_bod];
// Define the quaternion matrix to save everything
quat_saved = zeros(4, size(t_cjd, 2));
// Check for eclipse - dont know if I will be using this 
eclipse_ratio = CL_gm_eclipseCheck(sat_pos_EME, sun_pos_EME)';
eclipse_check = (eclipse_ratio == 1); //= 1 if eclipse and 0 if not
// Find the solar angle. Note: I am setting the angle to develop the quaternions to be saved as the AEM files
for j = 1:length(t_cjd)
    // Set a random error with max of pi/3"       
    err = (%pi/6)*rand()*(2*(rand()>0.5)-1)
    // Define the angle 
    angle_norm_sun = CL_vectAngle(mat*panel_1_normal,relsun_pos_eme(:,j)/norm(relsun_pos_eme(:,j)))
    // Find the axis that we need to rotate to get to this attitude 
    axis_to_rotate = CL_cross(mat*panel_1_normal, relsun_pos_eme(:,j)/norm(relsun_pos_eme(:,j)))
    quat = CL_rot_axAng2quat(axis_to_rotate,err+angle_norm_sun);
    quat_saved(1,j) = real(quat)
    quat_saved(2:4,j) = imag(quat)
    w = CL_rot_rotVect(quat,mat*panel_1_normal);
    if j == 2
        disp(mat*panel_1_normal)
        disp(w)
    end
end

// Set the first quaternion a little off 
//quat_saved(2,1) = 0.1001
//quat_saved(3,1) = 0.5001
//quat_saved(4,1) = 0.010101

//Eclipse duration computation
res(i) = CL_gm_eclipse(sat_eph(1,:),sat_eph(2,:),sat_eph(3,:),sat_eph(4,:),sat_eph(5,:),alpha_sun,delta_sun);
//disp(res)
//max_eclipse_duration(i) = max(res(i).duration);
//mean_eclipse_duration(i) = mean(res(i).duration);
//    
for i = 1:size(orbits_to_study, 2)
    period = CL_kp_params('per',orbits_to_study(1,i));
end
//
//
// Save this data in the required format 
filename = env_path + "../dataFiles/Period.txt';
result_concat = 'period: '+string(period)
fd = mopen(filename,'wt');
mputl(result_concat, fd);
mclose(fd);

// Save the panel normals 
filename = env_path + "../dataFiles/panel_normals.txt"
result1 = 'Panel 1 Normal:,' 
result2 = 'Panel 2 Normal:,' 
result3 = 'Panel 3 Normal:,'
result4 = 'Panel 4 Normal:,'  
for i = 1:3
    result1 = result1 + string(panel_1_normal(i)) + ',';
    result2 = result2 + string(panel_2_normal(i)) + ',';
    result3 = result3 + string(panel_3_normal(i)) + ',';
    result4 = result4 + string(panel_4_normal(i)) + ',';
end
result = [result1;result2;result3;result4]
disp(result)
fd = mopen(filename,'wt');
mputl(result, fd);
mclose(fd);

// Save the OEM file 
filename = env_path + "../dataFiles/OEM.txt";
result_concat = [];
for i = 1:length(sat_pos(1,:))
    result = '';
    for r = 1:length(sat_pos(:,i))
        result = result + string(sat_pos(r,i));
        
        result = result + ","
       
    end
    result_concat = [result_concat; result];
end

fd = mopen(filename,'wt');
mputl(result_concat, fd);
mclose(fd);

// Save the OEM file 
filename = env_path + "../dataFiles/OEM_SUN.txt";
result_concat = [];
for i = 1:length(sun_pos(1,:))
    result = '';
    for r = 1:length(sun_pos(:,i))
        result = result + string(sun_pos(r,i));
        
        result = result + ","
       
    end
    result_concat = [result_concat; result];
end

fd = mopen(filename,'wt');
mputl(result_concat, fd);
mclose(fd);

// Save the AEM file 
filename = env_path + "../dataFiles/AEM.txt"
result_concat = [];
for i = 1:length(quat_saved(1,:))
    result = '';
    for r = 1:length(quat_saved(:,i))
        result = result + string(quat_saved(r,i));

        result = result + ","
        
    end
    result_concat = [result_concat; result];
end

fd = mopen(filename,'wt');
mputl(result_concat, fd);
mclose(fd);

// Save the representation of the x axis body axis in terms of relative ECI/ EME2000 reference frame
filename = env_path + "../dataFiles/x_representation.txt"
result_concat = ['Relative EME2000 Reference Frame for x axis of body reference frame (unit vector),']
for i = 1:length(x_bod)
    result_concat = [result_concat; string(x_bod(i))+',']
end
fd = mopen(filename,'wt');
mputl(result_concat, fd);
mclose(fd);




