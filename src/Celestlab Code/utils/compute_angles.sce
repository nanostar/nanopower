//This function computes the angles between each face of the satellite and any astral body. 
//Inputs:
// body_pos_EME : astral body position in EME frame (3xN)
// sat_pos_EME : satellite position in EME frame (3xN)
// quat_sat_to_EME : quaternion describing the satellite attitude (1xN)
//Outputs
// angles : angles between +X, +Y, +Z, -X, -Y, -Z and the astral body (6xN)
function angles = compute_angles(body_pos_EME, sat_pos_EME, quat_sat_to_EME)
    
    px_EME = CL_rot_rotVect(quat_sat_to_EME, [1; 0; 0]);
    py_EME = CL_rot_rotVect(quat_sat_to_EME, [0; 1; 0]);
    pz_EME = CL_rot_rotVect(quat_sat_to_EME, [0; 0; 1]);
        
    //Computation of the angles between the axis and the Sun direction 
    angles(1,:) = CL_vectAngle(body_pos_EME-sat_pos_EME, px_EME);
    angles(2,:) = CL_vectAngle(body_pos_EME-sat_pos_EME, py_EME);
    angles(3,:) = CL_vectAngle(body_pos_EME-sat_pos_EME, pz_EME);    
    angles(4,:) = CL_vectAngle(body_pos_EME-sat_pos_EME, -px_EME);
    angles(5,:) = CL_vectAngle(body_pos_EME-sat_pos_EME, -py_EME);
    angles(6,:) = CL_vectAngle(body_pos_EME-sat_pos_EME, -pz_EME); 
    
endfunction
