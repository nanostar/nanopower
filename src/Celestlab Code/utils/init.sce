funcprot(0);
clear;
env_path = fullpath(get_absolute_file_path("init.sce") + "../");
exec(env_path + "utils/array2str.sci", -1);
exec(env_path + "utils/cic_ecrire.sce", -1);
exec(env_path + "Configuration/Orbits/OrbitsList.sce", -1);
exec(env_path + "Configuration/GroundStations/GroundStations.sce", -1);
exec(env_path + "utils/check_inside.sci", -1);

stacksize max;
