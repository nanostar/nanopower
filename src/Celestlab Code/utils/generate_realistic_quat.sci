//This function creates a realistic quaternion ephemeris, i.e. an ephemeris that mixes the single-mode ephemeris to simulate the satellie switching modes. 
//Inputs
// sat_pos_EME: satellite position in the inertial frame (double 3xN)
// quat_mission: satellite attitude quaternion in the mission mode (quat 1xN)
// quat_standby: satellite attitude quaternion in the standby mode (quat 1xN)
// quat_dl: satellite attitude quaternion in the downlink mode (quat 1xN)
// ratio_mission: "payload duty cycle", double in [0,1] giving the time proportion of the mission mode
// mission_slot_duration: duration of a mission mode interval in time steps (double)
// angle_SP_sun_mission, standby and dl: angles between the solar panels face and the Sun for each mode (double 1xN)
//Outputs
// quat: mixed quaternion (quat 1xP)
// modes: mode indicator (integer 1xP). 1=Mission, 2=Standby, 3=Downlink
// angle_SP_Sun: mixed angle between the solar panels face and the Sun (double 1xP)

function [quat, modes, angle_SP_Sun] = generate_realistic_quat(t_cjd, sat_pos_EME, quat_mission, quat_standby, quat_dl, ratio_mission, mission_slot_duration, angle_SP_Sun_mission, angle_SP_Sun_standby, angle_SP_Sun_dl)
    
    mode_switch = zeros(3, length(t_cjd)); //3xN matrix. Each line corresponds to a mode and for each column.A 1 is written in the line corresponding to the current satellite mode.

    //--------- Mission and standby ----------
    n = round(ratio_mission*(length(t_cjd))/mission_slot_duration); //number of switches between mission and standby
    t_mode_change = t_cjd(1:mission_slot_duration:$); //time steps where a switch between mission and standby may occur
    mode_slot = 2-(rand(1:length(t_mode_change)) < ratio_mission); //for each time step, decide with a ratio_mission probability between mission and standby

	//The mode changes only occured in 1 out of mission_slot_duration timesteps, these two lines duplicates the result to "fill" the gaps
    temp = repmat(mode_slot', 1, mission_slot_duration)'; 
    mode_change = temp(:)'; //Line vector of 1s and 2s
    
    
    mode_switch(1, mode_change(1:length(t_cjd)) == 1) = 1; //Set the mission mode 
    mode_switch(2, mode_change(1:length(t_cjd)) == 2) = 1; //Set the standby mode
    
	//--------- Downlink ---------------------
	
    //Determination of dates in a pass
    sat_pos_ecf = CL_fr_convert("EME2000", "ECF", t_cjd, sat_pos_EME);
    [interv] = CL_ev_stationVisibility(t_cjd, sat_pos_EME, [%pi/180*1.48; %pi/180*43.56;147], CL_deg2rad(10.6));
    [elev] = CL_gm_stationPointing([%pi/180*1.48; %pi/180*43.56;147], sat_pos_ecf, "elev");
    
    timesteps_in_pass = find(elev > CL_deg2rad(10.6));
    mode_switch(3, timesteps_in_pass) = 1; //All timesteps in a pass are given the downlink quaternion
    mode_switch(1, timesteps_in_pass) = 0; //Remove mission timesteps to avoid conflict
    mode_switch(2, timesteps_in_pass) = 0; //Remove standby timesteps to avoid conflict
    
    //---------- Quaternion merging ----------
    quat = mode_switch(1,:).*quat_mission + mode_switch(2,:).*quat_standby + mode_switch(3,:).* quat_dl();
    
    angle_SP_Sun = mode_switch(1,:).*angle_SP_Sun_mission + mode_switch(2,:).*angle_SP_Sun_standby + mode_switch(3,:).*angle_SP_Sun_dl;
    modes = mode_switch(1,:) + 2*mode_switch(2,:) + 3*mode_switch(3,:);
endfunction
