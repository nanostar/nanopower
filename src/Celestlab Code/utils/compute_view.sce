//This function computes whether each face of the satellite sees the Sun or the Earth
//Inputs:
// ang_sun : angles between all the faces and the Sun (output of compute_angles, 6xN)
// ang_earth : angles between all the faces and the Earth (output of compute_angles, 6xN)
//Outputs:
// sun_in_view : 1 if the face sees the Sun, 0 if not (6xN)
// earth_in_view : 1 if the face sees the Earth, 0 if not (6xN)
function [sun_in_view, earth_in_view] = compute_view(ang_sun, ang_earth)
    
    sun_in_view = abs(ang_sun) < %pi/2;
    earth_in_view = abs(ang_earth) < %pi/2;
    
endfunction
