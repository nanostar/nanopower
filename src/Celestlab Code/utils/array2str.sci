// This function converts an array to a string (the same that would be displayed by disp)
// Inputs:
// - array: array to convert (1xN)
// Outputs:
// - str: string
function str = array2str(array)
    str = "[" + string(array(1));
    if(size(array,2) >= 2)
        for i = 2:size(array,2)
            str = str + "," + string(array(i));
        end
    end
    str = str + "]";
endfunction
