// Fonction to check if a point is in a geographical area. 

function [bool] = check_inside(point, area1, area3)
    long = point(1,:);
    lat = point(2,:);
    // cas qui permet de gerer les discontinuites au meridien
    if area3(1) < area1(1) then
        bool = (lat < area1(2) & lat > area3(2)) & ...
               ((long > area1(1) & long < 2*%pi) | ...
                (long > 0 & long < area3(1)));
    else // cas normal
        bool = (lat < area1(2) & lat > area3(2)) & ...
               (long > area1(1) & long < area3(1));
    end
endfunction
