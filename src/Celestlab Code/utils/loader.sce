// ---------------------------------
// Loader file for SIMU-CIC
// Author : CNES
// ---------------------------------

try
  // Load library
  simuciclib = lib(fullfile(get_absolute_file_path("loader.sce"), "lib")); 

  // Add menu to SCILAB console
  simucic_addMenu(); 
  
catch
  mprintf("\nUnabled to load SIMU-CIC \n\n"); 
end



