//This file defines all the ground stations that can be used. 
//4xN : longitude (rad), latitude (rad), altitude (m), min elevation (deg) 

ground_stations.isae = [%pi/180*1.50; %pi/180*43.43; 255;10*%pi/180]; // Ground station(UHF/VHF & S band) located at ISAE Supaéro

//ground_stations.kou = [%pi/180*-51.9; %pi/180*4.9; 0;10*%pi/180]; // Station sol (UHF/VHF & S-band) located in Kourou


