//This file defines all the orbits that are to be studied.

orbits = struct();

cjd0 = CL_dat_cal2cjd(2020,12,1,0,0,0);

orbits.cjd0 = cjd0;
orbits.SSO_650_10h30 = [6378000+650000;0.01;CL_deg2rad(97.9);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',10.5, 'ra'); CL_deg2rad(0)]; // NIMPH reference orbit
// Possible orbits (semimajor axis; eccentricity; inclination; argument of periapsis; longitude of the ascending node; mean anomaly at epoch).
//orbits.SSO_650_1h00 = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 1.0, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_3h00 = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 3.0, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_5h45 = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 5.75, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_6h00 = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 6.0, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_7h00 = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 7, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_8h00 = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 8, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_9h00 = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',9, 'ra'); CL_deg2rad(0)];

//orbits.SSO_650_9h30 = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 9.5, 'ra'); CL_deg2rad(0)];
// Altitude change
//orbits.SSO_400_1h = [6378000+400000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',1, 'ra'); CL_deg2rad(0)];
//orbits.SSO_400_2h = [6378000+400000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',2, 'ra'); CL_deg2rad(0)];
//orbits.SSO_400_3h15 = [6378000+400000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',3.25, 'ra'); CL_deg2rad(0)];
//orbits.SSO_400_0h = [6378000+400000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',0, 'ra'); CL_deg2rad(0)];
//orbits.SSO_400_9h = [6378000+400000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',9, 'ra'); CL_deg2rad(0)];
//orbits.SSO_400_10h30 = [6378000+400000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 10.5, 'ra'); CL_deg2rad(0)];
//orbits.SSO_500_4h = [6378000+500000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 4, 'ra'); CL_deg2rad(0)];
//orbits.SSO_500_5h = [6378000+500000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 5, 'ra'); CL_deg2rad(0)];
//orbits.SSO_500_10h30 = [6378000+500000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 10.5, 'ra'); CL_deg2rad(0)];
//orbits.SSO_550_10h30 = [6378000+550000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 10.5, 'ra'); CL_deg2rad(0)];
//orbits.SSO_500_6h = [6378000+500000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 6, 'ra'); CL_deg2rad(0)];
//orbits.SSO_500_9h = [6378000+500000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 9, 'ra'); CL_deg2rad(0)];
//orbits.SSO_600_10h30 = [6378000+600000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 10.5, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_10h30 = [6378000+650000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 10.5, 'ra'); CL_deg2rad(0)]; 
//orbits.SSO_700_6h = [6378000+700000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 10.5, 'ra'); CL_deg2rad(0)]; 
//orbits.SSO_700_7h = [6378000+700000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 7, 'ra'); CL_deg2rad(0)];
//orbits.SSO_700_7h30 = [6378000+700000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 7.5, 'ra'); CL_deg2rad(0)];
//orbits.SSO_700_8h = [6378000+700000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 8, 'ra'); CL_deg2rad(0)];
//orbits.SSO_700_10h30 = [6378000+700000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 10.5, 'ra'); CL_deg2rad(0)];
//orbits.SSO_800_8h = [6378000+800000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 8, 'ra'); CL_deg2rad(0)];
//orbits.SSO_800_10h30 = [6378000+800000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 10.5, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_11h = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',11, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_12h = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',12, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_14h = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',14, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_16h00 = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh', 16.0, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_17h = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',17, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_18h = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',18, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_21h = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',21, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_22h30 = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',22.5, 'ra'); CL_deg2rad(0)];
//orbits.SSO_650_23h = [6378000+650000;0.00;CL_deg2rad(98);CL_deg2rad(0);CL_op_locTime(cjd0, 'mlh',23, 'ra'); CL_deg2rad(0)];
// 500km

//
//orbits.SSO_550_1h = [6378000+550000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 1, 'ra'); CL_deg2rad(0)];
//orbits.SSO_550_2h = [6378000+550000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 2, 'ra'); CL_deg2rad(0)];
//
//orbits.SSO_550_3h = [6378000+550000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 3, 'ra'); CL_deg2rad(0)];
//orbits.SSO_550_4h = [6378000+550000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 4, 'ra'); CL_deg2rad(0)];
//orbits.SSO_550_5h = [6378000+550000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 5, 'ra'); CL_deg2rad(0)];
//
//orbits.SSO_550_6h = [6378000+550000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh',6, 'ra'); CL_deg2rad(0)];
//S
//orbits.SSO_550_14h = [6378000+500000; 0.001; CL_deg2rad(97.6); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 14, 'ra'); CL_deg2rad(0)];
//
//orbits.SSO_600_6h = [6378000+600000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 6, 'ra'); CL_deg2rad(0)];
//orbits.SSO_600_7h = [6378000+600000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 7, 'ra'); CL_deg2rad(0)];
//
//orbits.SSO_600_5h45 = [6378000+600000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 5.75, 'ra'); CL_deg2rad(0)];
//
//orbits.SSO_600_5h = [6378000+600000; 0.00; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 5, 'ra'); CL_deg2rad(0)];
//
//orbits.SSO_600_12h = [6378000+500000; 0.001; CL_deg2rad(97.8); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 12, 'ra'); CL_deg2rad(0)];
//
//orbits.SSO_650_6h = [6378000+500000; 0.001; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 6, 'ra'); CL_deg2rad(0)];
//
//orbits.SSO_650_12h = [6378000+500000; 0.001; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 12, 'ra'); CL_deg2rad(0)];
//
//orbits.SSO_650_14h = [6378000+500000; 0.001; CL_deg2rad(98); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 14, 'ra'); CL_deg2rad(0)];
//
//orbits.ISS = [6378000+406000; 0.0003314; CL_deg2rad(98.15); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh', 10.5, 'ra'); CL_deg2rad(0)];

//Give me some inclination change
//orbits.Polar_650_6h = [6378000+650000; 0.00; CL_deg2rad(90); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh',6, 'ra'); CL_deg2rad(0)];
//orbits.incline45_650_6h = [6378000+650000; 0.00; CL_deg2rad(45); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh',6, 'ra'); CL_deg2rad(0)];
//orbits.incline60_650_6h = [6378000+650000; 0.00; CL_deg2rad(60); CL_deg2rad(0); CL_op_locTime(cjd0, 'mlh',6, 'ra'); CL_deg2rad(0)];








