# Testing obtaining the solar angle matrix and checking with celestlab
from utils import find_solar_eclipse_matrix
import configparser
import math
import numpy
# Set solar angle to always be 30 degrees
# Read the configparser
config = configparser.ConfigParser()
no_of_panels = 4
config.read('example.ini')
OEM_file = config['Satellite and SUN OEM and AEM']['Sat OEM File']
AEM_file = config['Satellite and SUN OEM and AEM']['Sat AEM File']
Sun_OEM_file = config['Satellite and SUN OEM and AEM']['Sun OEM File']
panel_normal_file = config['Panel and Solar Arrangement']['Panel Normal File']
x_rep_file = config['Satellite and SUN OEM and AEM']['Body x representation in relative basis file']
solar_angle_matrix, eclipse_indicator = find_solar_eclipse_matrix(no_of_panels, OEM_file, AEM_file, Sun_OEM_file,
                                                                  panel_normal_file, x_rep_file)
solar_angle_matrix = solar_angle_matrix.astype(numpy.float)
eclipse_indicator = eclipse_indicator.astype(numpy.float)
# Convert Radians to degrees
solar_angle_matrix_deg = solar_angle_matrix * (180/math.pi)

print(solar_angle_matrix_deg)
print(eclipse_indicator)