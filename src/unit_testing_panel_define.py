# Unit testing for panel define and organise from the power subsystem
import math
# Define the inputs required
solararrangement = [[2, 3], [2, 6], [5, 3]]
max_cells = [6, 6, 14, 7]
no_of_panels = len(max_cells)
solar_angle = [math.pi/6, math.pi/4, math.pi/1.2, math.pi/6]

# Mention this algorithm of placing the cells takes no geometric considerations into account

# Import the function and call it
from panel_define_furtherworktodo import paneldef
solar_arrangement, panel_definition = paneldef(solararrangement, max_cells, no_of_panels)
print('solar arrangement is now '+str(solar_arrangement))
print('panel definition is ' + str(panel_definition))


