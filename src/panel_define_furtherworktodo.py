def paneldef(solararrangement, max_cells, no_of_panels):
    import numpy
    # Check the user isnt a goose
    tot_cells = 0
    for arrangement in solararrangement:
        s = arrangement[0]
        p = arrangement[1]
        tot_cells += s*p

    if tot_cells == sum(max_cells):
        # Start of by sorting via nice fits
        countsaved = []
        # Go through each element in the solar arrangement
        for i in numpy.arange(0,len(solararrangement),1):
            # Define the parallel cells
            p = solararrangement[i][1]
            s = solararrangement[i][0]
            count = 0
            for k in numpy.arange(0,no_of_panels,1):
                if max_cells[k]%p == 0:
                    count +=1
                if max_cells[k]%s == 0:
                    count += 1
            countsaved.append(count)

        solararrangement = [x for _, x in sorted(zip(countsaved,solararrangement),reverse=True)]

        # Define parameters used in the checking process
        canfitmore = [True]*no_of_panels
        solarfitted = [False]*len(solararrangement)

        taken_panels = [False]*no_of_panels
        panel_align = []
        panel = []

        for j in list(range(0,no_of_panels)):
            panel.append(j)#eclipse_ind[j])#'P'+str(j))

        # Go through all the solararrangement looking for smug fit
        for i in list(range(0,len(solararrangement))):
            panel_align.append('NotSort')

            for k in list(range(0,len(max_cells))):
                if not (solarfitted[i]):
                    # Extract series and parallel first
                    s = solararrangement[i][0]
                    p = solararrangement[i][1]
                    # Find the number of cells in this arrangement
                    no_of_cells = s*p

                    # Now check if any of the arrangements fit nicely
                    if no_of_cells == max_cells[k]:
                        # This means it fit all smug and nice so set the
                        if not(taken_panels[k]):
                            list1 = [panel[k]]*s*p
                            taken_panels[k] = True
                            panel_align[i] = list1
                            canfitmore[k] = False
                            solarfitted[i] = True

        needtotestmore = False
        print(panel_align)
        for val in panel_align:
            if val == 'NotSort':
                needtotestmore = True
        if needtotestmore:

            # For each cell now we finding where to put it in a panel
            stored_indx = []
            max_cells_untaken = []
            cells_left_in_list = []
            for i in numpy.arange(0, len(taken_panels),1):
                if taken_panels[i]==False:
                    stored_indx.append(i)
                    max_cells_untaken.append(max_cells[i])
                    cells_left_in_list.append(max_cells[i])
                else:
                    cells_left_in_list.append(0)

            # Sort stored_indx according to corresponding max cells array
            stored_indx = [x for _, x in sorted(zip(max_cells_untaken, stored_indx), reverse=True)]

            indxforindx = 0
            current_panel_indx = stored_indx[indxforindx]

            left_already_decided = False
            space = []
            for i in numpy.arange(0,len(solararrangement),1):
                s = solararrangement[i][0]
                p = solararrangement[i][1]
                sp = s*p
                space.append(sp)

            # Go through each arrangement in solar arrangement
            for i in numpy.arange(0, len(solararrangement),1):
                #print('new solar arrangement')
                # Don't want to touch the stuff thats already good
                if panel_align[i] == 'NotSort':
                    panel_align[i] = []
                    s = solararrangement[i][0]
                    p = solararrangement[i][1]

                    # Only going to test series cells
                    test_fit = s

                    # Define panels to save which is a way to save info
                    panels_to_save = []

                    #if not(left_already_decided):
                     #   left_of_panel = max_cells[current_panel_indx]

                    # Go through all the parallels to see how many sets can fit
                    for j in list(range(0,p)):
                        print('j is '+str(j))
                        print(cells_left_in_list)
                        # Go through all the indices
                        for indx in list(range(0, len(stored_indx))):
                            # Check if there is a panel left with enough to satisfy the test fit
                            if cells_left_in_list[stored_indx[indx]] >= test_fit:
                                indxforindx = indx
                                print('index for index is ' + str(indxforindx))
                                current_panel_indx = stored_indx[indxforindx]
                                # Also want to reset leftofit
                                left_of_panel = cells_left_in_list[current_panel_indx]
                                left_already_decided = True
                                # Break the loop
                                break
                        print('left is '+str(left_of_panel))
                        # Check if each of the series sets can fit
                        if test_fit <= left_of_panel:
                            # If so I want to update the panel_align with this
                            panels_to_save = panels_to_save + [panel[current_panel_indx]] * test_fit

                            # Okay save these panels in the corresponding index of the arrangement
                            panel_align[i] = panels_to_save

                            # Update how much is left
                            left_of_panel = left_of_panel - test_fit
                            print(left_of_panel)
                            # Check the panel align
                            print(panel_align)

                        # Then we want to check if what is left is too small for the next fit
                        cells_left_in_list[current_panel_indx] = left_of_panel
                        print(cells_left_in_list)
                #print(panel_align)
                space[i] = space[i] - len(panel_align[i])
                print('space is '+str(space))
            # Now we want to check if there is a space that is not zero
            totestfurther = False
            indx_s = 0
            to_test_indx = []
            print(solararrangement)
            print(panel_align)
            for val in space:
                if not(val==0):
                    totestfurther = True
                    to_test_indx.append(indx_s)
                indx_s += 1
            # Check if further placement is required
            if totestfurther:
                # We want fill out each of the spaces
                for indx_m in list(range(0, len(cells_left_in_list))):
                    # Ensure that it is not 0
                    if not(cells_left_in_list[indx_m] == 0):
                        # Then set the test fit as this value
                        test_fit2 = cells_left_in_list[indx_m]
                        # Now we want to find where there is space
                        for indx_n in list(range(0, len(space))):
                            # Check that space is not 0
                            if not(space[indx_n] == 0):
                                # Now Check that the test fit is smaller or equal to what is left in the space
                                if test_fit2 <= space[indx_n]:
                                    # Save this to the corresponding panel align
                                    panel_align[indx_n] = panel_align[indx_n]+[indx_m]*test_fit2
                                    # Reduce the amount of left over space
                                    space[indx_n] = space[indx_n] - test_fit2
                                    print(panel_align)
            print(space)
    else:
        print('You a Goose')
        raise ValueError('You A Goose: Please ensure solar arrangements line up with max panels')
    #print(panel_align)
    return solararrangement, panel_align