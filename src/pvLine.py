# PvLine Class
class pvLine:
    def __init__(self,s,p,voltage,T,destroyed_cells,angle,eclipseind,solarcell,diode,current,power):
        self.s = s
        self.p = p
        self.T = T
        self.voltage = voltage
        self.destroyed_cells = destroyed_cells
        self.solarcell = solarcell
        self.angle = angle
        self.diode = diode
        self.eclipseind = eclipseind
        self.current_now = current
        self.power_now = power

def get_pVobject_from_voltage(s, p, voltage_req, T, destroyed_cells, solarangle_now, eclipseind, solarcell, thediode, pvconverter):
    # This is for each solar arrangment, go through each series set for parrallel cells

    # For each series line the voltage is added up, but if one of the cells if off then the line cannot produce any
    # any current and is effectively useless (not producing any power) so the only cells that are going to produce
    # current is when the whole series line is on i.e. each cell produces voltage required divided by s
    import numpy
    v_req_each_cell = voltage_req / s

    count_to_s = 0
    I_series_set = []
    I_parallel_total = 0
    for j in list(range(0, len(eclipseind))):
        if eclipseind[j] == 0:
            # current is off
            I_series_set.append(0)
        else:
            # Otherwise find the current
            I_series_set.append(solarcell.calc_current(v_req_each_cell, T, thediode, solarangle_now[j]))
        # Find the minimum of the current sources
        I_min = min(I_series_set)
        count_to_s += 1
        # If Im up to s
        if count_to_s == s:
            # Add on the minimum
            I_parallel_total += I_min
            # Reset I
            I_series_set = []
            #print(I_min)
            # Reset count to s
            count_to_s = 0
    # Store the current
    current_now = I_parallel_total
    # Now find the power
    power_now = I_parallel_total * voltage_req

    # Find the power with the efficiency from the converter if it is in place
    if not (pvconverter == 0):
        eta = pvconverter.geteffiencyfrominputpower(power_now)*0.01
        #print(eta)
        power_now = eta * power_now

    return pvLine(s, p, voltage_req, T, destroyed_cells, solarangle_now, eclipseind, solarcell, thediode, current_now,
                  power_now)

def get_pVobject_MPPT(s, p, T, destroyed_cells, solarangle_now, eclipseind, solarcell, thediode, EPS, pvconverter):
    #print('#####')
    # This is for each solar arrangment, go through each series set for parrallel cells
    import numpy
    # First define the voltage array
    voltage_array = numpy.arange(0,EPS.Vmax+EPS.MPPTincrement,EPS.MPPTincrement)

    # Define the lists
    currentlist = []
    powerlist = []

    for voltage_req in voltage_array:
        #print('****')
        # Look above for reasoning
        v_req_each_cell = voltage_req / s

        count_to_s = 0
        I_series_set = []
        I_parallel_total = 0
        for j in list(range(0, len(eclipseind))):
            #print('is it 1 ' + str(eclipseind[j] == 1))
            if eclipseind[j] == 0:
                #print('settted')
                # current is off
                I_series_set.append(0)
            else:
                # Otherwise find the current
                I_series_set.append(solarcell.calc_current(v_req_each_cell, T, thediode, solarangle_now[j]))
            # Find the minimum of the current sources
            I_min = min(I_series_set)
            count_to_s += 1
            # If Im up to s
            if count_to_s == s:
                # Add on the minimum
                #print('current for each series set is '+str(I_min))
                I_parallel_total += I_min
                # Reset I
                I_series_set = []

                # Reset count to s
                count_to_s = 0
            # Store the current
            current_now = I_parallel_total
            # Now find the power
            power_now = I_parallel_total * voltage_req
        # Save the current and power into their lists
        currentlist.append(current_now)
        powerlist.append(power_now)

    # Now we want to take the maximum power
    power = max(powerlist)
    indx = powerlist.index(power)
    voltage = voltage_array[indx]
    current = currentlist[indx]

    if not (pvconverter == 0):
        # Find the efficiency factor and convert to decimal
        eta = pvconverter.geteffiencyfrominputpower(power) * 0.01
        #print(eta)
        power = power * eta

    return pvLine(s, p, voltage, T, destroyed_cells, solarangle_now, eclipseind, solarcell, thediode, current, power)



