# User must create his own battery mode function or excel table: seems like developing their own function might be too hard
import csv
import sys
# GONNA MAKE THIS INTO A CLASS
class extra_battery_mode:
    def __init__(self,name,hardware_yn=False,extra_effect_norm = False,extra_effect_itself = False,importance = 'NaN',require_yn = False,input_yn = False,entry_condition=[],exit_condition=[],req_effect={},in_effect={},state='OFF'):
        self.hardware_yn = hardware_yn
        self.entry_condition= entry_condition
        self.exit_condition = exit_condition
        self.req_effect = req_effect
        self.in_effect = in_effect
        self.extra_effect_norm = extra_effect_norm
        self.extra_effect_itself = extra_effect_itself
        self.require_yn = require_yn
        self.input_yn = input_yn
        self.importance = importance
        self.state = state
        self.name = name


def get_batterymode_from_file(filename):
    import os
    entry_condition = []
    exit_condition = []
    req_effect = []
    in_effect = []
    hardware_yn = False
    extra_effect_norm = False
    extra_effect_itself = False
    importance = 'NaN'
    require_yn = False
    input_yn = False
    initial_state = 'OFF'
    f = open(os.path.join("dataFiles",filename), 'rU')
    reader = csv.reader(f)
    tally = 0
    name = 'NaN'
    for row in reader:
        # Read the required effect and input effect statements
        if tally < 1:
            name = row[1]
        if tally < 2:
            yn = row[1]
            if yn == 'TRUE':
                hardware_yn = True
            else:
                hardware_yn = False
        elif tally < 3:
            yn = row[1]
            if yn == 'TRUE':
                extra_effect_norm=True
            else:
                extra_effect_norm = False
        elif tally < 4:
            yn = row[1]
            if yn == 'TRUE':
                extra_effect_itself = True
            else:
                extra_effect_itself = False
        elif tally < 5:
            importance = row[1]
        elif tally < 6:
            yn = row[1]
            if yn == 'TRUE':
                require_yn = True
            else:
                require_yn = False
        elif tally < 7:
            iyn = row[1]
            if iyn == 'TRUE':
                input_yn = True
            else:
                input_yn = False
        # Take out the region we care about
        if tally > 7 and tally < 13:
            #print(row)
            if not (row[1] == 'NaN'):
                entry_condition.append([row[0], row[1]])
                exit_condition.append([row[0], row[2]])
        # Now we want to extract the effects
        if tally > 13 and tally < 16:
            #print(row)
            if require_yn:
                if not (row[1] == 'NaN'):
                    req_effect.append([row[0],row[1]])
            if input_yn:
                if not (row[2] == 'NaN'):
                    in_effect.append([row[0],row[2]])
        # Extract initial state
        if tally == 16:
            #print(row)
            initial_state = row[1]
        # Add onto tally so we can extract everything
        tally += 1
    f.close()
    return extra_battery_mode(name,hardware_yn,extra_effect_norm,extra_effect_itself,importance,require_yn,input_yn,entry_condition,exit_condition,req_effect,in_effect,initial_state)

# If extra battery modes are required please create the excel sheet as listed in the documentation and fill out the
# following list with the names of the files (note: please ensure files are .csv)
def obtainadditionalmodes(extra_modes):
    # Sort through the itself battery modes via importance
    sort_import = []
    sort1 = []
    sort2 = []
    sort3 = []
    # Define the additional modes
    for mode in extra_modes:
        extramode = get_batterymode_from_file(mode)
        sort_import.append(extramode)
        # Organise these
        if extramode.hardware_yn:
            sort1.append(extramode)
        elif extramode.extra_effect_norm:
            sort2.append(extramode)
        else:
            sort3.append(extramode)
     # Sort these
    import1 = []
    if not(sort1[0].importance=='NaN'):
        for mode in sort1:
            # Define the importance list
            import1.append(mode.importance)
        # Now sort out sort1 based on import1
        sorted1 = [x for _,x in sorted(zip(import1,sort1),reverse=True)]
    else:
        sorted1 = sort1
    import2 = []
    if not (sort2[0].importance == 'NaN'):
        for mode in sort2:
            # Define the importance list
            import2.append(mode.importance)
        # Now sort out sort1 based on import1
        sorted2 = [x for _, x in sorted(zip(import2, sort2), reverse=True)]
    else:
        sorted2 = sort2
    #print(sorted2)
    import3 = []
    if not (sort3[0].importance == 'NaN'):
        for mode in sort3:
            # Define the importance list
            import3.append(mode.importance)
        # Now sort out sort1 based on import1
        sorted3 = [x for _, x in sorted(zip(import3, sort3), reverse=True)]
    else:
        sorted3 = sort3
    return sorted1+sorted2+sorted3


#file = 'hardwareProtectionModeLowVoltage.csv'
#battery_mode = get_batterymode_from_file(file)
#extraprint(battery_mode.name)
#file = 'softwareProtectionMode_SAFE.csv'
#battery_mode = get_batterymode_from_file(file)
#print(battery_mode.name)
#print(battery_mode.hardware_yn)
# Im going to need a state vector that has mode, voltage, current and power
#state_vector_base = ['Current Mode', 'Voltage','Current','Power']
#for i in list(range(0,len(state_vector_base))):
   # for element in entry_condition1:
  #      if state_vector_base[i] == element:
 #           print("this will let me compare the right values")

# Context for how you plan on approaching the condition
#apple = 5
#combine_str = str(apple)+entry_condition2[1]
#if eval(combine_str):
#    print("lo")

