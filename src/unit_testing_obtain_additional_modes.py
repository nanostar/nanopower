# Unit testing function to obtain the additional modes of the EPS
from batterymode import obtainadditionalmodes
import configparser

# Read the configparser
config = configparser.ConfigParser()
config.read('example.ini')

extra_modes = config['Battery Protection Modes']['Extra mode file']
if not(extra_modes == []):
    extra_modes = extra_modes.split(',')
    additional_modes = obtainadditionalmodes(extra_modes)
else:
    additional_modes = extra_modes

print(additional_modes)
for extra in additional_modes:
    print(extra.name)
    print(extra.input_yn)
    print(extra.hardware_yn)