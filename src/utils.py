# Utils file


# Reading satellite mode table, please input a csv file with sat modes as shown in example
def read_sat_modes(filename):
    import csv
    import sys
    import os
    file = open(os.path.join("dataFiles", filename), 'rU')
    reader = csv.reader(file)
    tally = 0

    # Define dictionary for each satellite mode
    sat_mode = {}
    mode_array = []
    # Go through rows and make a dictionary of each satellite mode, each definition is a list with the first component
    # being the platform mode and the second being the payload mode
    for row in reader:
        if tally < 1:
            for ele in row[1:]:
                sat_mode[ele] = []
                mode_array.append(ele)
        elif tally < 3:
            for i in list(range(1, len(row))):
                sat_mode[mode_array[i-1]].append(row[i])
        tally += 1
    return sat_mode


# Define function to find the increment
def findnearestincrement(value, linspace_array):
    import numpy
    setvalue = max(abs(linspace_array))
    storedincrement = 0
    for i in numpy.arange(0, len(linspace_array), 1):
        if abs(value - linspace_array[i]) < setvalue:
            # Save this increment
            storedincrement = i
            # Reset setvalue
            setvalue = abs(value - linspace_array[i])

    return storedincrement


def tau_cycle_from_data(cyclefile, DOD, cycle, method='linear'):
    import numpy
    from openpyxl import load_workbook
    from scipy.interpolate import griddata
    #import matplotlib.pyplot as plt
    #cyclefile = "dataFiles\\accuCyclingData.xlsx"
    wb = load_workbook(cyclefile)
    ws = wb['Sheet1']
    #print('am i being called')
    # Define the arrays
    DOD_array = numpy.array([])
    cycles_array = numpy.array([])
    tau_array = numpy.array([])

    # Go through each row
    for row in ws.iter_rows():
        # Go through each cell
        for cell in row:
            # Ignore the title row
            if cell.row > 1:
                if cell.column == 'A':
                    # We are in DOD column here
                    DOD_array = numpy.append(DOD_array, float(cell.value))
                if cell.column == 'B':
                    # We are in Cycles column here
                    cycles_array = numpy.append(cycles_array, float(cell.value))
                if cell.column == 'C':
                    # We are in tau column here
                    tau_array = numpy.append(tau_array, float(cell.value))

    x = numpy.linspace(min(DOD_array),max(DOD_array),130)
    y = numpy.linspace(min(cycles_array),max(cycles_array),800)
    X, Y = numpy.meshgrid(x,y)

    #method = 'linear'
    Ti_solid = griddata((DOD_array, cycles_array), tau_array, (X, Y), method=method)


    # Find the nearest increment to the DOD and cycle value using the same function
    storedincrementDOD = findnearestincrement(DOD, x)
    storedincrementcycles = findnearestincrement(cycle,y)

    # Now output the value from the gridded values
    tau_value = Ti_solid[storedincrementcycles, storedincrementDOD]

    # Scale it to make sure
    tau_value = max(0,tau_value)
    tau_value = min(1,tau_value)

    return tau_value


# Age Function
def tau_age_from_data(agefile, Temp, SOC, age, method='linear'):
    import time
    start = time.time()
    from openpyxl import load_workbook
    from scipy.interpolate import griddata
    import numpy
    #agefile = 'accuLifeData.xlsx'
    wb = load_workbook(agefile)
    ws = wb['Sheet1']

    # Define the storing arrays
    age_array = numpy.array([])
    SOC_array = numpy.array([])
    Temp_array = numpy.array([])
    taulife_array = numpy.array([])

    # Go through each cell
    for row in ws.iter_rows():
        for cell in row:
            # Ignore the first row
            if cell.row > 1:
                # Save the temperature data
                if cell.column == 'A':
                    Temp_array = numpy.append(Temp_array, float(cell.value))
                if cell.column == 'B':
                    SOC_array = numpy.append(SOC_array, float(cell.value))
                if cell.column == 'C':
                    age_array = numpy.append(age_array, float(cell.value))
                if cell.column == 'D':
                    taulife_array = numpy.append(taulife_array, float(cell.value))

    x = numpy.linspace(min(Temp_array), max(Temp_array), 250)
    y = numpy.linspace(min(SOC_array), max(SOC_array), 150)
    z = numpy.linspace(min(age_array), max(age_array), 200)

    X, Y, Z = numpy.meshgrid(x, y, z)

    Ti_solid = griddata((Temp_array, SOC_array, age_array), taulife_array, (X, Y, Z), method=method)
    end = time.time()
    print('time taken to obtain the data '+str(end-start)+'s')

    tempincrement = findnearestincrement(Temp, x)
    socincrement = findnearestincrement(SOC, y)
    ageincrement = findnearestincrement(age, z)

    # Try obtain the value for tau
    tau_value = Ti_solid[socincrement, tempincrement, ageincrement]
    tau_value = max(0,tau_value)
    tau_value = min(1,tau_value)

    return tau_value


# Lets find the conditioning
def get_bus_dict_list(file_name, sat_mode_file, buses=['3.3V','5V']):
    import math
    saved_row_pay = math.inf
    saved_row_plat = math.inf
    #file_name = 'InputPowerConditioning.xlsx'
    #sat_mode_file = "SatelliteModes.csv"

    # Find the element modes from the sat mode file
    sat_modes = read_sat_modes(sat_mode_file)
    payload_modes = []
    plaform_modes = []

    from openpyxl import load_workbook
    import os
    file = os.path.join("dataFiles", file_name)
    wb = load_workbook(file)
    ws = wb['Sheet1']
    #buses = ['3.3V','5V']
    row_def = False
    col_def = False
    col_def_pay = False
    row_def_pay = False
    bus_dict_list = []
    for bus in buses:
        bus_dict = {}
        for k, v in sat_modes.items():
            power_consumed = 0
            sat_mode = k
            plat_mode = v[0]
            pay_mode = v[1]

            # Go through the cells to find the power consumed
            for row in ws.iter_rows():
                for cell in row:
                    # Ignore the first row
                    if cell.row > 1:
                        # Now find the platform power consumption row
                        if cell.value == 'PLATFORM':
                            # Save this row
                            saved_row_plat = cell.row

                        # Find the payload power consumption row
                        if cell.value == 'PAYLOAD':
                            # Save this row
                            saved_row_pay = cell.row

                        # Save the power consumed by platform if the row number is in between the platform and payload row
                        if saved_row_plat < cell.row < saved_row_pay:
                            if cell.value == plat_mode:
                                # Save the column
                                col_number = cell.column
                                col_def = True
                            # Check if the cell matches the required bus
                            if cell.value == bus:
                                # Save this row
                                row_number = cell.row
                                row_def = True

                        # Save the power consumed by payload if the row number is greater than the payload row
                        if cell.row > saved_row_pay:
                            # Check if the cell matches the payload mode
                            if cell.value == pay_mode:
                                # Save the column
                                col_number_pay = cell.column
                                col_def_pay = True
                            # Check if the cell matches the required bus
                            if cell.value == bus:
                                # Save this row
                                row_number_pay = cell.row
                                row_def_pay = True

                if row_def and col_def:
                    val = ws[col_number + str(row_number)].value

                    power_consumed += val
                if row_def_pay and col_def_pay:
                    val1 = ws[col_number_pay + str(row_number_pay)].value
                    power_consumed += val1
                # After each row reset row defined
                row_def = False
                row_def_pay = False
            # Save this power consumed as part of the dictionary
            bus_dict[sat_mode] = power_consumed
        # After each value is saved reset col defined
        col_def = False
        col_def_pay = False
        # Save the dictionary
        bus_dict_list.append(bus_dict)
    return bus_dict_list


def get_conso_dict(sat_mode_filename, filename, power_condition):
    if power_condition == 'SIMPLE':
        IDM_CIC_file = filename
        conso_dictionary = consumption_IDM_CIC(sat_mode_filename, IDM_CIC_file)
    else:
        conso_dictionary = get_bus_dict_list(filename, sat_mode_filename)
    return conso_dictionary


def consumption_IDM_CIC(sat_mode_filename, IDM_CIC_file):
    import os
    filename = sat_mode_filename
    sat_modes = read_sat_modes(filename)
    sat_mode_array = []
    for k, v in sat_modes.items():
        sat_mode_array.append(k)

    # Now define the conso table from the IDM-CIC (power budget)
    from openpyxl import load_workbook
    power_conso_file = os.path.join("datafiles", IDM_CIC_file)
    wb = load_workbook(power_conso_file)
    ws = wb['System power budget']
    sat_power = {}
    for search_name in sat_mode_array:
        # Extract the power consumed
        for row in ws.iter_rows():
            for cell in row:
                #print(cell)
                # Figure out which row to look for power
                if cell.value == 'Total consumed power without any margin':
                    row_number = cell.row

                # Figure out which columns to look for power
                if cell.value == search_name:
                    col_number = cell.column

        # Pick out value and store in a dictionary
        sat_power[search_name] = ws[col_number + str(row_number)].value
    return sat_power


# Default mission plan via excel system
def missionPlan_excel(filename,orbit_period):
    import os
    # Define the missionPlan for testing
    from openpyxl import load_workbook
    filename = os.path.join("datafiles", filename)
    wb = load_workbook(filename)
    ws = wb['Sheet1']
    # Go through each row to determining the starting time with reference to 0 at which the satellite changes modes
    mode_start_time = []
    for row in ws.iter_rows():
        start_time = 0
        for cell in row:
            if not(type(cell.value) is str):
                # Add on the number of orbits before activation of mode
                if cell.column == 'A':
                    start_time += cell.value*orbit_period
                # Add on the number of minutes
                if cell.column == 'B':
                    start_time += cell.value*60
                # Add on the number of seconds
                if cell.column == 'C':
                    start_time += cell.value
            if cell.column == 'D' and cell.row > 1:
                mode_start_time.append([cell.value,start_time])
    return mode_start_time


def paneldef(solararrangement, max_cells, no_of_panels):
    import numpy
    # Check the user isnt a goose
    tot_cells = 0
    for arrangement in solararrangement:
        s = arrangement[0]
        p = arrangement[1]
        tot_cells += s*p

    if tot_cells == sum(max_cells):
        # Start of by sorting via nice fits
        countsaved = []
        # Go through each element in the solar arrangement
        for i in numpy.arange(0,len(solararrangement),1):
            # Define the parallel cells
            p = solararrangement[i][1]
            s = solararrangement[i][0]
            count = 0
            for k in numpy.arange(0,no_of_panels,1):
                if max_cells[k]%p == 0:
                    count +=1
                if max_cells[k]%s == 0:
                    count += 1
            countsaved.append(count)

        solararrangement = [x for _, x in sorted(zip(countsaved,solararrangement),reverse=True)]

        # Define parameters used in the checking process
        canfitmore = [True]*no_of_panels
        solarfitted = [False]*len(solararrangement)

        taken_panels = [False]*no_of_panels
        panel_align = []
        panel = []

        for j in list(range(0,no_of_panels)):
            panel.append(j)#eclipse_ind[j])#'P'+str(j))

        # Go through all the solararrangement looking for smug fit
        for i in list(range(0,len(solararrangement))):
            panel_align.append('NotSort')

            for k in list(range(0,len(max_cells))):
                if not (solarfitted[i]):
                    # Extract series and parallel first
                    s = solararrangement[i][0]
                    p = solararrangement[i][1]
                    # Find the number of cells in this arrangement
                    no_of_cells = s*p

                    # Now check if any of the arrangements fit nicely
                    if no_of_cells == max_cells[k]:
                        # This means it fit all smug and nice so set the
                        if not(taken_panels[k]):
                            list1 = [panel[k]]*s*p
                            taken_panels[k] = True
                            panel_align[i] = list1
                            canfitmore[k] = False
                            solarfitted[i] = True

        needtotestmore = False
        #print(panel_align)
        for val in panel_align:
            if val == 'NotSort':
                needtotestmore = True
        if needtotestmore:

            # For each cell now we finding where to put it in a panel
            stored_indx = []
            max_cells_untaken = []
            cells_left_in_list = []
            for i in numpy.arange(0, len(taken_panels),1):
                if taken_panels[i]==False:
                    stored_indx.append(i)
                    max_cells_untaken.append(max_cells[i])
                    cells_left_in_list.append(max_cells[i])
                else:
                    cells_left_in_list.append(0)

            # Sort stored_indx according to corresponding max cells array
            stored_indx = [x for _, x in sorted(zip(max_cells_untaken, stored_indx), reverse=True)]

            indxforindx = 0
            current_panel_indx = stored_indx[indxforindx]

            left_already_decided = False
            space = []
            for i in numpy.arange(0,len(solararrangement),1):
                s = solararrangement[i][0]
                p = solararrangement[i][1]
                sp = s*p
                space.append(sp)

            # Go through each arrangement in solar arrangement
            for i in numpy.arange(0, len(solararrangement),1):
                #print('new solar arrangement')
                # Don't want to touch the stuff thats already good
                if panel_align[i] == 'NotSort':
                    panel_align[i] = []
                    s = solararrangement[i][0]
                    p = solararrangement[i][1]

                    # Only going to test series cells
                    test_fit = s

                    # Define panels to save which is a way to save info
                    panels_to_save = []

                    #if not(left_already_decided):
                     #   left_of_panel = max_cells[current_panel_indx]

                    # Go through all the parallels to see how many sets can fit
                    for j in list(range(0,p)):
                        #print('j is '+str(j))
                        #print(cells_left_in_list)
                        # Go through all the indices
                        for indx in list(range(0, len(stored_indx))):
                            # Check if there is a panel left with enough to satisfy the test fit
                            if cells_left_in_list[stored_indx[indx]] >= test_fit:
                                indxforindx = indx
                                #print('index for index is ' + str(indxforindx))
                                current_panel_indx = stored_indx[indxforindx]
                                # Also want to reset leftofit
                                left_of_panel = cells_left_in_list[current_panel_indx]
                                left_already_decided = True
                                # Break the loop
                                break
                        #print('left is '+str(left_of_panel))
                        # Check if each of the series sets can fit
                        if test_fit <= left_of_panel:
                            # If so I want to update the panel_align with this
                            panels_to_save = panels_to_save + [panel[current_panel_indx]] * test_fit

                            # Okay save these panels in the corresponding index of the arrangement
                            panel_align[i] = panels_to_save

                            # Update how much is left
                            left_of_panel = left_of_panel - test_fit
                            #print(left_of_panel)
                            # Check the panel align
                            #print(panel_align)

                        # Then we want to check if what is left is too small for the next fit
                        cells_left_in_list[current_panel_indx] = left_of_panel
                        #print(cells_left_in_list)
                #print(panel_align)
                space[i] = space[i] - len(panel_align[i])
                #print('space is '+str(space))
            # Now we want to check if there is a space that is not zero
            totestfurther = False
            indx_s = 0
            to_test_indx = []
            #print(solararrangement)
            #print(panel_align)
            for val in space:
                if not(val==0):
                    totestfurther = True
                    to_test_indx.append(indx_s)
                indx_s += 1
            # Check if further placement is required
            if totestfurther:
                # We want fill out each of the spaces
                for indx_m in list(range(0, len(cells_left_in_list))):
                    # Ensure that it is not 0
                    if not(cells_left_in_list[indx_m] == 0):
                        # Then set the test fit as this value
                        test_fit2 = cells_left_in_list[indx_m]
                        # Now we want to find where there is space
                        for indx_n in list(range(0, len(space))):
                            # Check that space is not 0
                            if not(space[indx_n] == 0):
                                # Now Check that the test fit is smaller or equal to what is left in the space
                                if test_fit2 <= space[indx_n]:
                                    # Save this to the corresponding panel align
                                    panel_align[indx_n] = panel_align[indx_n]+[indx_m]*test_fit2
                                    # Reduce the amount of left over space
                                    space[indx_n] = space[indx_n] - test_fit2
                                    #print(panel_align)
            #print(space)
    else:
        print('You a Goose')
        raise ValueError('You A Goose: Please ensure solar arrangements line up with max panels')
    #print(panel_align)
    return solararrangement, panel_align


def obtain_OEM_data(file):
    # Open the file
    import os
    filename = os.path.join("dataFiles", file)
    f = open(filename, "r")
    # Read the contents
    contents = f.read()
    f.close()
    # Split the data based on the comma delimmiter
    data = contents.split(',')
    tot_saved = []
    saved_set = []
    tally = 0
    for value in data:
        if not(value == '\n'):
            val = float(value)
            tally += 1
            if tally < 3:
                # save the data to the set
                saved_set.append(val)
            elif tally == 3:
                # Save the data and the set
                saved_set.append(val)
                tot_saved.append(saved_set)
            else:
                # Reset tally and saved saved set
                saved_set = []
                # Save the data and the set
                saved_set.append(val)
                tally = 1
    return tot_saved


def obtain_AEM_data(file):
    # Open the file
    import os
    filename = os.path.join("dataFiles", file)
    f = open(filename, "r")
    # Read the contents
    contents = f.read()
    f.close()
    # Split the data based on the comma delimmiter
    data = contents.split(',')
    tot_saved = []
    saved_set = []
    tally = 0
    for value in data:
        if not(value == '\n'):
            val = float(value)
            tally += 1
            if tally < 4:
                # save the data to the set
                saved_set.append(val)
            elif tally == 4:
                # Save the data and the set
                saved_set.append(val)
                tot_saved.append(saved_set)
            else:
                # Reset tally and saved saved set
                saved_set = []
                # Save the data and the set
                saved_set.append(val)
                tally = 1
    return tot_saved


def obtain_Sun_OEM_data(file):
    # Open the file
    #file = 'OEM_SUN.txt'
    import os
    filename = os.path.join("dataFiles", file)
    f = open(filename, "r")
    # Read the contents
    contents = f.read()
    f.close()
    # Split the data based on the comma delimmiter
    data = contents.split(',')
    for i in list(range(0, len(data))):
        dat = data[i]
        dat = dat.replace('D', 'e')
        data[i] = dat
    tot_saved = []
    saved_set = []
    tally = 0
    for value in data:
        if not (value == '\n'):
            val = float(value)

            tally += 1
            if tally < 3:
                # save the data to the set
                saved_set.append(val)
            elif tally == 3:
                # Save the data and the set
                saved_set.append(val)
                tot_saved.append(saved_set)
            else:
                # Reset tally and saved saved set
                saved_set = []
                # Save the data and the set
                saved_set.append(val)
                tally = 1
    return tot_saved


def reading_relative_x_bod(file):
    import os
    #file = 'x_representation.txt'
    filename = os.path.join("dataFiles", file)
    f = open(filename, "r")
    # Read contents
    contents = f.read()
    f.close()
    # Split the data based on the comma delimmiter
    data = contents.split(',')
    tally = 0
    list = []
    for value in data:
        if not (value == '\n') and tally > 0:
            val = float(value)
            list.append(val)
        tally += 1
    return list


def read_panel_normals(no_of_panels,file):
    import os
    import numpy as np
    #file = 'panel_normals.txt'
    filename = os.path.join("dataFiles", file)
    f = open(filename, "r")
    # Read contents
    contents = f.read()
    f.close()
    # Split this data with the space delimiter first
    first_data = contents.split(',')
    tally = 0
    saved = np.zeros([3,no_of_panels])
    i = 0
    m = -1
    for data in first_data:
        if not(tally%4 == 0):
            saved[i,m] = data
            i +=1
        else:
            # Otherwise we want to move down a row and reset i
            m += 1
            i = 0

        tally += 1
    return saved


def get_matrix_representation(file):
    import numpy as np
    # Get the x body reference representation in the relative EME2000 reference frame
    x_bod = reading_relative_x_bod(file)
    x_bod = np.array(x_bod)

    # Follow the convention matrix to develop the y and z representations
    Rz = np.array([[0,-1,0], [1,0,-1], [0,0,1]])
    Ry = np.array([[0, 0, 1],[0, 1, 0],[-1, 0, 0]])
    y_bod =Rz.dot(x_bod)
    z_bod = np.linalg.inv(Ry).dot(x_bod)
    mat = np.array([[x_bod[0], y_bod[0], z_bod[0]], [x_bod[1], y_bod[1], z_bod[1]], [x_bod[2], y_bod[2], z_bod[2]]])
    return mat


def unit_vector(vector):
    from numpy import linalg as LA
    vec = vector/LA.norm(vector)
    return vec


def angle_between(v1, v2):
    import numpy as np
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def q_mult(q1, q2):
    w1, x1, y1, z1 = q1
    w2, x2, y2, z2 = q2
    w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
    x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
    y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2
    z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2
    return w, x, y, z


def q_conjugate(q):
    w, x, y, z = q
    return (w, -x, -y, -z)


def qv_mult(q1, v1):
    import numpy
    q2 = numpy.append(0.0, v1)
    return q_mult(q_mult(q1, q2), q_conjugate(q1))[1:]


def find_intersection_solid_angle(alpha, alpha1, alpha2):
    import math
    # A celestlab function that allows for the calculation of the solid angle of the intersection of 2 cones
    # Sort the alpha
    alpha_1 = min(alpha1, alpha2)
    alpha2 = max(alpha1, alpha2)
    alpha1 = alpha_1

    # Find the cos of these alphas as it will be used in the calculation of the solid angle
    ca1 = math.cos(alpha1)
    ca2 = math.cos(alpha2)

    # Check if the small cone is within the large cone via the geometric condition below:
    if alpha <= alpha2 - alpha1:
        solid_angle = 2 * math.pi * (1 - ca1)
    # Check if there is no intersection at all
    elif alpha >= alpha1 + alpha2:
        solid_angle = 0
    else:
        # Otherwise we fall into the standard case
        X = (alpha1 + alpha2 + alpha) / 2
        sina1 = math.sin(X - alpha1)
        sina2 = math.sin(X - alpha2)
        sina = math.sin(X - alpha)
        R = math.sqrt(sina1 * sina2 * sina / math.sin(X))

        beta1 = 2 * math.atan(R / sina2)
        beta2 = 2 * math.atan(R / sina1)
        betaa = 2 * math.atan(R / sina)

        # Find the solid angle
        solid_angle = 2 * (beta1 * (1 - ca1) + beta2 * (1 - ca2) - (betaa + beta1 + beta2 - math.pi))
    return solid_angle


def find_solar_eclipse_matrix(no_of_panels, OEM_file, AEM_file, SUN_OEM_file, panel_normal_file, x_rep_file):
    from numpy import linalg as LA
    import numpy as np
    import math
    panel_normals = read_panel_normals(no_of_panels, panel_normal_file)
    OEM_data = obtain_OEM_data(OEM_file)
    AEM_data = obtain_AEM_data(AEM_file)
    sun_OEM_data = obtain_Sun_OEM_data(SUN_OEM_file)

    # Set the storing lists for the solar angle matrix and the eclipse indicator matrix
    solar_angle_matrix = []
    eclipse_ind_matrix = []

    # First I need to check if the satellite position is behind Earth (that way everything is in eclipse)
    sr1 = 6.960e8  # Radius of Sun in m
    sr2 = 6378136.3  # Radius of Earth in m
    # Check the OEM data length
    #print('OEM data length is ' + str(len(OEM_data)))
    for k in list(range(0, len(OEM_data))):
        #current_OEM_unit = unit_vector(OEM_data[k])
        current_OEM = OEM_data[k]
        current_AEM = AEM_data[k]

        # Follow the celestlab code to find the eclipse indicator for whole satellite
        pos_obs = np.array(current_OEM)
        pos1 = np.array(sun_OEM_data[k])
        pos2 = np.array([0, 0, 0])

        # Find the norms
        norm1 = LA.norm(pos_obs - pos1)
        norm2 = LA.norm(pos_obs - pos2)

        # Find the semi apex angles of the cones
        alpha1 = math.asin(sr1 / norm1)
        alpha2 = math.asin(sr2 / norm2)

        # Find the angle between Sun and earth centres through the satellite
        alpha = angle_between(pos1 - pos_obs, pos2 - pos_obs)

        # Find the solid angle intersection between the 2 cones
        solid_angle = find_intersection_solid_angle(alpha, alpha1, alpha2)

        # Find the total solid angle
        solid_total = 2 * math.pi * (1 - math.cos(alpha1))

        # Find the ratio
        eclip_ratio = solid_angle / solid_total
        #print(eclip_ratio)
        # Now if the ratio is 1 we are totally in eclipse otherwise we are not
        if eclip_ratio == 1:
            eclipse = True
        else:
            eclipse = False

        eclipse_ind = []
        solar_angle = []

        # Find the matrix representation for going from the body reference frame to the relative EME2000 reference frame
        matrix = get_matrix_representation(x_rep_file)

        # Find the relative sun position based on the OEM
        relative_sun = pos1 - pos_obs
        # Note: In this case we have kept the body reference frame constant and have quaternions from the AEM file to
        # represent the rotations done to normal vectors (these rotations are in the EME reference frame)

        # Need the normal vectors in body reference
        for i in list(range(0, no_of_panels)):
            # Obtain the panels normal in body reference frame
            normal_panel = panel_normals[:, i]

            # Check if in eclipse
            if eclipse:
                # This means we set all the panel eclipse indicator to 0 (solar angle becomes not important anymore) -
                # so set as nan
                eclipse_ind.append(0)
                solar_angle.append('NaN')
            else:
                # Otherwise the whole satellite is not in eclipse but we still need to check whether the solar panel is
                # in a shadow
                # Shadow: Defined when the angle between the normal of the solar panel and the sun vector is greater
                # than pi/2

                # Note: pos1 is the current sun vector in EME reference frame while the panel normal is in body refereence
                # frame
                # To transform, need the vector corresponding to the x (first element) direction in the body reference frame
                # (need to change the basis)

                # Find the normal vector in the relative EME reference frame (view from satellite)
                norm_eme_rel = matrix.dot(normal_panel)

                # Rotate this normal vector in accordance to quaternion in the AEM file
                rotated_normal = qv_mult(current_AEM, norm_eme_rel)

                # Find the solar angle
                solar_angle_panel = angle_between(rotated_normal, relative_sun)
                solar_angle.append(solar_angle_panel)

                # Check if the solar angle is greater than 90 degrees
                if solar_angle_panel >= math.pi / 2:
                    # If this is the case, then the panel is in the shadow
                    eclipse_ind.append(0)
                else:
                    # Otherwise we are not in eclipse:
                    eclipse_ind.append(1)
        # Store the solar angle and eclipse ind into their matrices
        solar_angle_matrix.append(solar_angle)
        eclipse_ind_matrix.append(eclipse_ind)

    # Convert the lists into numpy matrices/ arrays
    solar_angle_matrix = np.array(solar_angle_matrix)
    eclipse_ind_matrix = np.array(eclipse_ind_matrix)

    # Return the matrices
    return solar_angle_matrix, eclipse_ind_matrix
