# Testing the eclipse indicator method
# Assumption I am making here is that the parallel cells want to remain grouped together for ease of wiring
def organise_solar_eclipse(solararrangement,eclipse_ind,max_cells,no_of_panels,solar_angle):
    import math
    #solararrangement = [[1,7],[2,3],[2,5]]
    #eclipse_ind = [1,1,0,1]
    # Define the maximum number of cells on each panel that matters (place in descending order)
    #max_cells = [6,4,6,7]
    #solar_angle = [math.pi/6,math.pi/4,math.pi/1.2,math.pi/6]
    #no_of_panels = 4
    import numpy
    # Check the user isnt a goose
    tot_cells = 0
    for arrangement in solararrangement:
        s = arrangement[0]
        p = arrangement[1]
        tot_cells += s*p

    if tot_cells == sum(max_cells):
        # Start of by sorting via nice fits
        countsaved = []
        # Go through each element in the solar arrangement
        for i in numpy.arange(0,len(solararrangement),1):
            # Define the parallel cells
            p = solararrangement[i][1]
            count = 0
            for k in numpy.arange(0,no_of_panels,1):
                if max_cells[k]%p == 0:
                    count +=1
            countsaved.append(count)
        solararrangement = [x for _, x in sorted(zip(countsaved,solararrangement),reverse=True)]

        # Define parameters used in the checking process
        canfitmore = [True]*no_of_panels
        solarfitted = [False]*len(solararrangement)

        taken_panels = [False]*no_of_panels
        ecliptopanel =[]
        solartopanel = []
        panel = []
        panel1 = []
        for j in list(range(0,no_of_panels)):
            panel.append(eclipse_ind[j])#eclipse_ind[j])#'P'+str(j))
            panel1.append(solar_angle[j])
        # Go through all the solararrangement looking for smug fit
        for i in list(range(0,len(solararrangement))):
            ecliptopanel.append('NotSort')
            solartopanel.append('NotSort')
            for k in list(range(0,len(max_cells))):
                if not (solarfitted[i]):
                    # Extract series and parallel first
                    s = solararrangement[i][0]
                    p = solararrangement[i][1]
                    # Find the number of cells in this arrangement
                    no_of_cells = s*p

                    # Now check if any of the arrangements fit nicely
                    if no_of_cells == max_cells[k]:
                        # This means it fit all smug and nice so set the
                        if not(taken_panels[k]):
                            list1 = [panel[k]]*s*p
                            list3 = [panel1[k]]*s*p
                            taken_panels[k] = True
                            ecliptopanel[i] = list1
                            solartopanel[i] = list3
                            canfitmore[k] = False
                            solarfitted[i] = True
                    # 6 satisfies everything fucking hell
        #print(ecliptopanel)
        needtotestmore = False
        for val in ecliptopanel:
            if val == 'NotSort':
                needtotestmore = True
        if needtotestmore:
            # For each cell now we finding where to put it in a panel
            stored_indx = []
            for i in numpy.arange(0,len(taken_panels),1):
                if taken_panels[i]==False:
                    stored_indx.append(i)
            indxforindx = 0
            current_panel_indx = stored_indx[indxforindx]
            #print(current_panel_indx)
            left_already_decided = False
            space = []
            for i in numpy.arange(0,len(solararrangement),1):
                s = solararrangement[i][0]
                p = solararrangement[i][1]
                sp = s*p
                space.append(sp)
            #print(space)
            # Go through each arrangement in solar arrangement
            for i in numpy.arange(0,len(solararrangement),1):
                # Don't want to touch the stuff thats already good
                if ecliptopanel[i] == 'NotSort':
                    s = solararrangement[i][0]
                    p = solararrangement[i][1]

                    if left_already_decided == False:
                        lefttofit = max_cells[current_panel_indx]
                    # Try fitting set of series cells
                    test_fit = s
                    #print('test fit is ' + str(test_fit))
                    panels_to_save = []
                    panel1_to_save = []
                    # Try fit sets of the parallel in
                    for j in numpy.arange(0,p,1):
                        #print('left is ' + str(lefttofit))
                        # Try fit a parallel set in first
                        if test_fit <= lefttofit:
                            # If this is the case, I want to save the panels used
                            panels_to_save = panels_to_save + [panel[current_panel_indx]]*test_fit
                            panel1_to_save = panel1_to_save + [panel1[current_panel_indx]] * test_fit
                            # Okay save these panels in the corresponding index of the arrangement
                            ecliptopanel[i] = panels_to_save
                            solartopanel[i] = panel1_to_save

                            # Now define how much is left in this panel
                            lefttofit = lefttofit-test_fit
                            # Also define how many space there are left to fit

                            # test fit can continue being p for now to see how many sets we can put into each panel
                            #print('left is ' + str(lefttofit))
                            #print(ecliptopanel)

                        else:
                            # So now test_fit is larger than whats left, so lets set whats left in the new index
                            test_fit2 = lefttofit
                            # Save the panels
                            panels_to_save = panels_to_save + [panel[current_panel_indx]] * test_fit2
                            panel1_to_save = panel1_to_save + [panel1[current_panel_indx]] * test_fit2
                            ecliptopanel[i] = panels_to_save
                            solartopanel[i] = panel1_to_save

                            # Now define whats left
                            lefttofit = lefttofit - test_fit2
                            #print('left is ' + str(lefttofit))
                            #print('i is ' + str(i))
                            #print(ecliptopanel)
                        # Now we want to move up an index when lefttofit hits 0
                        if lefttofit == 0:

                            indxforindx += 1
                            # Make sure index does not exceed the maximum
                            if not (indxforindx == len(stored_indx)):
                                #print('index for index is ' + str(indxforindx))
                                current_panel_indx = stored_indx[indxforindx]
                                # Also want to reset leftofit
                                lefttofit = max_cells[current_panel_indx]
                                left_already_decided = True
                                #print('current index is '+ str(current_panel_indx))
                    if not(space[i]==0):
                        #print(lefttofit)
                        #print('Something Has to be done')
                        # Check if the solar arrangment has already been satisfied
                        if len(ecliptopanel[i]) == s*p:
                            # Set left_already_decided to be true
                            left_already_decided = True
                            #print('left already decided')
                        else:
                            test_fit3 = space[i] - len(ecliptopanel[i])
                            #print('test fit is ' + str(test_fit3))
                            if test_fit3 <= lefttofit:
                                panels_to_save = panels_to_save + [panel[current_panel_indx]] * test_fit3
                                panel1_to_save = panel1_to_save + [panel1[current_panel_indx]] * test_fit3
                                # Okay save these panels in the corresponding index of the arrangement
                                ecliptopanel[i] = panels_to_save
                                solartopanel[i] = panel1_to_save
                                # Now define how much is left in this panel
                                lefttofit = lefttofit - test_fit3
                                left_already_decided = True
                                # Also define how many space there are left to fit

                                # test fit can continue being p for now to see how many sets we can put into each panel
                                #print('left is ' + str(lefttofit))
                                #print(ecliptopanel)
                    space[i] = space[i] - len(ecliptopanel[i])
                    #print('Space left is ' + str(space[i]))




        #print(ecliptopanel)

        #print(solartopanel)



        # Again go through all the solar arrangements
        #list2 = []
        #current_panel_indx = taken_panels[-1]+1
        #sumed = 0
        #for i in list(range(0,len(solararrangement))):
        #    # Except this time only care about the non sorted ones
        #    if ecliptopanel[i] == 'NotSort':
        #        # Now we are looking to fill out each panel (making the assumption that they havent provided incorrect inputs)
        #        s = solararrangement[i][0]
        #        p = solararrangement[i][1]

        #        for j in numpy.arange(0,s,1):
         #           print(j)
          #          if not (solarfitted[i]):
           #             # Check if a single set of parallel will fit
            #            sumed +=p
             #           if sumed <= max_cells[current_panel_indx ]:
              #              list2.append(panel[current_panel_indx])
               #             print(list2)
                #            print(sumed)
                 #       elif sumed > max_cells[current_panel_indx ]:
                  #          # Move to the next panel
                   #         current_panel_indx +=1
                    #        sumed = p
                     #       # Check if it fits the next panel
                      #      if sumed < max_cells[current_panel_indx]:
                       #         list2.append(panel[current_panel_indx])
                        #    print(sumed)
                    #print(list2)

                # Now we save list2
                #ecliptopanel[i] = list2
                #print(ecliptopanel)

                # Reset list2
         #       list2 = []

    #print(ecliptopanel)
    else:
        print('You a Goose')
        raise ValueError('You A Goose: Please ensure solar arrangements line up with max panels')
    return (solararrangement,ecliptopanel,solartopanel)






