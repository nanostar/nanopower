# If extra battery modes are required please create the excel sheet as listed in the documentation and fill out the
# following list with the names of the files (note: please ensure files are .csv)
def obtainadditionalmodes(extra_modes):
    from batterymode import get_batterymode_from_file
    # Sort through the itself battery modes via importance
    sort_import = []
    sort1 = []
    sort2 = []
    sort3 = []
    # Define the additional modes
    for mode in extra_modes:
        extramode = get_batterymode_from_file(mode)
        sort_import.append(extramode)
        # Organise these
        if extramode.hardware_yn:
            sort1.append(extramode)
        elif extramode.extra_effect_norm:
            sort2.append(extramode)
        else:
            sort3.append(extramode)
     # Sort these
    import1 = []
    if not(sort1[0].importance=='NaN'):
        for mode in sort1:
            # Define the importance list
            import1.append(mode.importance)
        # Now sort out sort1 based on import1
        sorted1 = [x for _,x in sorted(zip(import1,sort1),reverse=True)]
    else:
        sorted1 = sort1
    import2 = []
    if not (sort2[0].importance == 'NaN'):
        for mode in sort2:
            # Define the importance list
            import2.append(mode.importance)
        # Now sort out sort1 based on import1
        sorted2 = [x for _, x in sorted(zip(import2, sort2), reverse=True)]
    else:
        sorted2 = sort2
    #print(sorted2)
    import3 = []
    if not (sort3[0].importance == 'NaN'):
        for mode in sort3:
            # Define the importance list
            import3.append(mode.importance)
        # Now sort out sort1 based on import1
        sorted3 = [x for _, x in sorted(zip(import3, sort3), reverse=True)]
    else:
        sorted3 = sort3
    return sorted1+sorted2+sorted3
