# Define the tau as a class which is a property of the battery, that way I dont have to keep calling the degradation
class tau_class:

    def __init__(self, cyclefile, agefile, method_cycle='linear', method_age='linear'):
        import numpy
        from openpyxl import load_workbook
        from scipy.interpolate import griddata
        # import matplotlib.pyplot as plt
        # cyclefile = "dataFiles\\accuCyclingData.xlsx"
        wb = load_workbook(cyclefile)
        ws = wb['Sheet1']
        # print('am i being called')
        # Define the arrays
        DOD_array = numpy.array([])
        cycles_array = numpy.array([])
        tau_array = numpy.array([])

        # Go through each row
        for row in ws.iter_rows():
            # Go through each cell
            for cell in row:
                # Ignore the title row
                if cell.row > 1:
                    if cell.column == 'A':
                        # We are in DOD column here
                        DOD_array = numpy.append(DOD_array, float(cell.value))
                    if cell.column == 'B':
                        # We are in Cycles column here
                        cycles_array = numpy.append(cycles_array, float(cell.value))
                    if cell.column == 'C':
                        # We are in tau column here
                        tau_array = numpy.append(tau_array, float(cell.value))

        self.x1 = numpy.linspace(min(DOD_array), max(DOD_array), 200)
        self.y1 = numpy.linspace(min(cycles_array), max(cycles_array), 850)
        X, Y = numpy.meshgrid(self.x1, self.y1)

        # method = 'linear'
        self.Ti_cycle = griddata((DOD_array, cycles_array), tau_array, (X, Y), method=method_cycle)

        wb = load_workbook(agefile)
        ws = wb['Sheet1']

        # Define the storing arrays
        age_array = numpy.array([])
        SOC_array = numpy.array([])
        Temp_array = numpy.array([])
        taulife_array = numpy.array([])

        # Go through each cell
        for row in ws.iter_rows():
            for cell in row:
                # Ignore the first row
                if cell.row > 1:
                    # Save the temperature data
                    if cell.column == 'A':
                        Temp_array = numpy.append(Temp_array, float(cell.value))
                    if cell.column == 'B':
                        SOC_array = numpy.append(SOC_array, float(cell.value))
                    if cell.column == 'C':
                        age_array = numpy.append(age_array, float(cell.value))
                    if cell.column == 'D':
                        taulife_array = numpy.append(taulife_array, float(cell.value))

        self.x2 = numpy.linspace(min(Temp_array), max(Temp_array), 350)
        self.y2 = numpy.linspace(min(SOC_array), max(SOC_array), 200)
        self.z2 = numpy.linspace(min(age_array), max(age_array), 300)

        X, Y, Z = numpy.meshgrid(self.x2, self.y2, self.z2)

        self.Ti_age = griddata((Temp_array, SOC_array, age_array), taulife_array, (X, Y, Z), method=method_age)

    def find_tau_cycle(self, DOD, cycle):
        from utils import findnearestincrement

        storedincrementDOD = findnearestincrement(DOD, self.x1)
        storedincrementcycles = findnearestincrement(cycle, self.y1)

        # Now output the value from the gridded values
        tau_value = self.Ti_cycle[storedincrementcycles, storedincrementDOD]

        # Scale it to make sure
        tau_value = max(0, tau_value)
        tau_value = min(1, tau_value)

        return tau_value

    def find_tau_age(self, Temp, SOC, age):
        from utils import findnearestincrement

        tempincrement = findnearestincrement(Temp, self.x2)
        socincrement = findnearestincrement(SOC, self.y2)
        ageincrement = findnearestincrement(age, self.z2)

        # Try obtain the value for tau
        tau_value = self.Ti_age[socincrement, tempincrement, ageincrement]
        tau_value = max(0, tau_value)
        tau_value = min(1, tau_value)

        return tau_value


        