# Unit testing function for MPPT
import math
import numpy
s = 3
p = 3
from converter import getpvconverterobject
from EPS import EPS
from solarcell import janus_solarcell
from diode import diode
thediode = diode()
EPS = EPS(8.3,0.1,'POWERCONVERTERS+BUSSES')
solarcell = janus_solarcell()
T = 50
filename = 'pv_test_data.csv'
pvconverter = getpvconverterobject(filename)
eclipseind = [1, 1, 1, 1, 1, 1, 1, 1, 1]
solarangle_now = (math.pi/6)*numpy.ones(9)
destroyed_p = numpy.zeros(4)

# Running function now
from pvLine import get_pVobject_MPPT
pvLine_object = get_pVobject_MPPT(s, p, T, destroyed_p, solarangle_now, eclipseind, solarcell, thediode, EPS, pvconverter)
print(pvLine_object.current_now)
print(pvLine_object.voltage)
print(pvLine_object.voltage*pvLine_object.current_now)
print(pvLine_object.power_now)